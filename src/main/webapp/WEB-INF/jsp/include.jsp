<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%
String service_name="ELisA REST API";
%>

<%
String service_version="v3.0";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style type="text/css">

/*********************/
/***** General *******/
/*********************/ 

body {
  font-family: Verdana, Helvetica, Arial, sans-serif;
  margin:0px 30px 0px 30px; padding:0px;
  width:  95%
}

div {
  /*line-height: 1.5em;*/
}

a {
  color: #008800;
  text-decoration: none;
  font-weight: bold;
}

h1 {
  margin: 0px;
  margin-bottom: .2em;
}

h2 {
  font-family: Verdana, Helvetica, Arial, sans-serif;
  margin-top: 8px;
  margin-bottom: 8px;
  padding: 6px;
  border-top: 1px solid #dddddd;
  border-bottom: 1px solid #dddddd;
  border-left: 2px solid #999999;
  border-right: 2px solid #999999;
  color: #007700;
  /*background-color: #eeeeee;*/
  font-weight: normal;
  font-size: larger;
}


 #messages {
font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
width:100%;
border-collapse:collapse;
}
#messages td, #messages th 
{
font-size:1em;
border:1px solid #98bf21;
padding:3px 7px 2px 7px;
}
#messages th 
{
font-size:1.1em;
text-align:left;
padding-top:5px;
padding-bottom:4px;
background-color:#A7C942;
color:#ffffff;
}
#messages tr.alt td 
{
color:#000000;
background-color:#EAF2D3;
}

.fix_width_column { width: 150px; }

/*
#directives tr.alt td 
{
color:#000000;
background-color:#EAF2D3;
}
*/
</style>