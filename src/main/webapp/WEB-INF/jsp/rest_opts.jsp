<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ELisA REST API</title>
</head>
<body>
	<h2>[ <%=service_name%> <%=service_version%> ] Message options for message type: "${mt}" </h2>
	<table id="messages">
		<tr>
		<th><b>Name</b></th>
		<th><b>Possible values</b></th>
		<th><b>Type</b></th>
		<th><b>Comment</b></th>
		<th><b># Inner Options</b></th>
		</tr>
		<c:forEach var="opt" items="${opt.list}">
			<tr>
				<td><a href="./opt/${opt.name}"> ${opt.name} </a></td>
				 <td>${opt.possibleValues}</td>
     			 <td>${opt.type}</td>
    			  <td>${opt.comment}</td>
    			  <td>${opt.childOptions.count}</td>
			<tr>
		</c:forEach>
	</table>

</body>
</html>
