<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ page import="javax.servlet.http.HttpUtils.*"%>
<%
    String myURI = request.getRequestURI();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ELisA REST API</title>
</head>
<body>
	<h2>
		[
		<%=service_name%>
		<%=service_version%>
		] ${messages.count} messages - <a href="${messages.prev}">
			&lt;&lt; </a> - <a href="${messages.next}"> &gt;&gt; </a>
	</h2>
	<table id="messages">
		<c:forEach var="message" items="${messages.messages}">
			<th class="fix_width_column"><a href="messages/${message.ID}">
					${message.ID}</a></th>
			<th>${message.date} - <a href="?author=${message.author}">
					${message.author}</a> - ${message.subject}
			</th>
			<tr>
				<td class="fix_width_column">Body</td>
				<td>${message.bodyHTML}</td>
			</tr>
			<tr class="alt">
				<td class="fix_width_column">Message Type</td>
				<td><a href="?message_type=${message.messageType}">
						${message.messageType}</a></td>
			</tr>
			<tr>
				<td class="fix_width_column">Systems Affected</td>
				<td><c:forEach var="sa" items="${message.systemAffected.list}">
						<a href="?systems_affected=${sa}"> ${sa}</a> |
       				</c:forEach></td>
			</tr>
			<tr class="alt">
				<td class="fix_width_column">Attachments</td>
				<td><c:forEach var="att" items="${message.attachments.list}">
						<a href="${att.link}"> ${att.fileName}</a> |
       				</c:forEach></td>
			</tr>

		</c:forEach>
	</table>

</body>
</html>