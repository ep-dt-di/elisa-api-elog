<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ELisA REST API</title>
</head>
<body>
	<h2>[ <%=service_name%> <%=service_version%> ] List of possible system affected</h2>
	<table id="messages">
		<tr>
		<th><b>System Affected</b></th>
		</tr>
		<c:forEach var="sa" items="${sa.list}">
			<tr>
				<td> ${sa} </td>
			<tr>
		</c:forEach>
	</table>

</body>
</html>