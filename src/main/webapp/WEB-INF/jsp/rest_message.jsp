<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ELisA REST API</title>
</head>
<body>
	<h2>[ <%=service_name%> <%=service_version%> ] </h2>  

	<table id="messages">

		<tr>
			<th class="fix_width_column">ID: ${message.ID}</th>
			<th><b> ${message.date} - ${message.author} -
					${message.subject}</b></th>
		</tr>
		<tr class="alt">
			<td class="fix_width_column">Author</td>
			<td>${message.author}</td>
		</tr>
		<tr>
			<td class="fix_width_column">Username</td>
			<td>${message.username}</td>
		</tr>
		<tr class="alt">
			<td class="fix_width_column">Date</td>
			<td>${message.date}</td>
		</tr>
		<tr>
			<td class="fix_width_column">Subject</td>
			<td>${message.subject}</td>
		</tr>
		<tr class="alt">
			<td class="fix_width_column">Body</td>
			<td>${message.bodyHTML}</td>
		</tr>
		<tr>
			<td class="fix_width_column">Message Type</td>
			<td>${message.messageType}</td>
		</tr>
		<tr class="alt">
			<td class="fix_width_column">System Affected</td>
			<td>${message.systemAffected.listString}</td>
		</tr>
		<tr>
			<td class="fix_width_column">Options</td>
			<td><c:forEach var="option"
					items="${message.optionalAttributes.list}">
         ${option.name}: ${option.value} |
      		 </c:forEach></td>
		</tr>
		<tr class="alt">
			<td class="fix_width_column">Attachments</td>
			<td><c:forEach var="att" items="${message.attachments.list}">
					<a href="${att.link}"> ${att.fileName}</a> |
       				</c:forEach></td>
		</tr>
		<tr>
			<td class="fix_width_column">Has Attachments</td>
			<td>${message.hasAttachments}</td>
		</tr>
		<tr class="alt">
			<td class="fix_width_column">Has Replies</td>
			<td>${message.hasReplies}</td>
		</tr>
		<tr>
			<td class="fix_width_column">Reply to</td>
			<td>${message.replyTo}</td>
		</tr>
		<tr class="alt">
			<td class="fix_width_column">Tread Head</td>
			<td>${message.threadHead}</td>
		</tr>
		<tr>
			<td class="fix_width_column">Valid</td>
			<td>${message.valid}</td>
		</tr>
		<tr class="alt">
			<td class="fix_width_column">Status</td>
			<td>${message.status}</td>
		</tr>



	</table>

</body>
</html>