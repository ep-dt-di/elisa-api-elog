<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ELisA REST API</title>
</head>
<body>
	<h2>[ <%=service_name%> <%=service_version%> ] Message types and metadata</h2>
	<table id="messages">
		<tr>
		<th><b>Message type name</b></th>
		<th><b>Options</b></th>
		<th><b>Preset System Affected</b></th>
		</tr>
		<c:forEach var="mt" items="${mt.list}">
			<tr>
				<td> ${mt} </td>
				<td><a href="./mt/${mt}/opt"> /opt </a></td>
				<td><a href="./mt/${mt}/sa"> /sa </a></td>
			<tr>
		</c:forEach>
	</table>

</body>
</html>