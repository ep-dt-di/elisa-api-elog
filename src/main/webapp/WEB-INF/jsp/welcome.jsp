<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>EliSA REST interface</title>
</head>
<body>
<h2> [ <%=service_name%> <%=service_version%> ]  Welcome </h2>
<p> 
The <a href="https://atlasop.cern.ch/elisa/defaultList.htm?logbook=ATLAS">ELisA </a> API is a RESTful service, which means it uses HTTP
requests to interact with ATLAS logbook messages. Clients can use ELisA's REST API to create, reply, insert, update and search among logbook messages.

<p> You can use any language or tool that supports HTTP calls to access the ELisA REST API. </p>

<p> Project documentation: <a href="https://atlasdaq.cern.ch/elisa_api_alpha/server.html#documentation"> https://atlasdaq.cern.ch/elisa_api_alpha/server.html#documentation</a> 
<p> REST interface specification, with functions specification and data formats: <a href="https://atlasdaq.cern.ch/elisa_api_alpha/protocol.html" > https://atlasdaq.cern.ch/elisa_api_alpha/protocol.html </a>  
<p> Jira: <a href="https://its.cern.ch/jira/browse/ELISA"> https://its.cern.ch/jira/browse/ELISA </a>
<p>

Your browser is an HTTP client at the end... use the REST interface to
<ul>
<li> <a href="./messages"> browse logbook messages </a> </li>
<li> <a href="./mt"> browse message metadata </a> </li>
</ul>

</body>
</html>