package elisa.service;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.regex.Pattern;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Whitelist;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.github.rjeschke.txtmark.Processor;;

/**
 * This class consists exclusively of static methods 
 * providing conversion facilities between HTML and
 * and the Markdown format ({@link http://en.wikipedia.org/wiki/Markdown})
 * used for log-message text representation in ELisA
 * 
 * @author lmagnoni, aradu
 *
 */

public class Utils {

	private final static Log log = LogFactory.getLog(elisa.service.Utils.class);
	
	
	/* 
	 * Character entity references in HTML (http://www.w3.org/TR/xhtml1/dtds.html#a_dtd_Special_characters)
	 * such as &nbsp, have to be translated in the numeric equivalent in order to be 
	 * correctly processed by the XSLT transformation.
	 * 
	 * To do that, the input HTML is prepended with a DOCTYPE definition 
	 * that contains all the character-entity -> numeric mapping as defined by W3C for
	 * HTML 4
	 * The mapping is contained in a dedicated utils class @see XHTML_encoding 
	 */
	private final String HTML_PREPEND = XHTML_Encoding.getDOCTYPE();
	
	
	/* 
	 * The XSLT stylesheet is loaded in memory in order to be reused 
	 * for many different transformation, as suggested:
	 * http://onjava.com/pub/a/oreilly/java/news/javaxslt_0801.html
	 */
	private Templates cachedXSLT;
	
	private Resource xsltFile = new ClassPathResource("el_html2md.xsl");
	
	public Utils() {
		//String xsltfile = getServletContext().getInitParameter("AdministratorEmail");
		try {
			Source xsltSource = new StreamSource(xsltFile.getFile().getAbsoluteFile());
			TransformerFactory transFact = TransformerFactory.newInstance();
			cachedXSLT = transFact.newTemplates(xsltSource);
		} catch (TransformerConfigurationException e) {
			log.error("Unable to read the XSLT transformation file! "+e);
		} catch (IOException e) {
			log.error("Unable to read the XSLT transformation file! "+e);
		}
	}
	
	
	/**
	 * This method transform a "dirty" HTML document coming from the 
	 * WYSWYG web editor in ELiSA in a properly formatted Markdown data. 
	 * It performs:
	 * <ul>
	 * 		<li> HTML 2 XHTML translation </li>
	 * 		<li> preserving initial formatting as possible, with "\n" to \<\br\> implicit conversion </li> 
	 * </ul>
	 * @param html The input HTML snippet
	 * @return representation in markdown format
	 */
	public String cleanhtml2markdown(String html) {
		log.trace("initial html:\n"+html);
		//HTML 2 XHTML transformation
		
		//Workaround for ORACLE NON-UTF8 CHARSET
		//Non existing bullet chars are manually transformed i dash
		html = html.replace("&#8211;", "-").replace("&#8226;", "-");
		
		Document dirty = Jsoup.parse(html);
		//Careful with the Whitelist! Everything is removed but the allowed tags!
		Cleaner cleaner = new Cleaner(Whitelist.relaxed());
		Document clean = cleaner.clean(dirty);
		clean.outputSettings().prettyPrint(false);
		//clean.outputSettings().escapeMode(EscapeMode.extended); //This is to preserve some character https://github.com/jhy/jsoup/issues/147
		
		String cleanedHTML = clean.body().html();
		cleanedHTML="<div>"+cleanedHTML+"</div>";
		log.trace("cleaned text:\n"+cleanedHTML);
		
		String md = html2markdown(cleanedHTML);
		
		//Automatic link creation
		//http://... or https://... strings are automatically included in a anchor <a> tag
		md = md.replaceAll("(?<![\\[\\(])(http[s]?://[a-zA-Z0-9+&@#/%?=~_|!:.;\\\\-]+)(?![\\[\\(])", "[$1]($1)");
		//Fix the problem for link with empty href created by tinyMice -> [htp://...]()
		md = md.replaceAll("(\\[)(http[s]?://[a-zA-Z0-9+&@#/%?=~_|!:.;\\\\-]+)(\\])(\\(\\))", "[$2]($2)");
		
		//unescape literal representation of HTML entities before putting into the database
		//so the email text is displayed correctly; useful for the REST interface as well 
		md = LiteralUnescaping(md);

		return md;
	}
	
	/**
	 * This method transform XHTML document in Markdown data.
	 * @param xhtml input document in XHTML
	 * @return markdown representation of input document
	 */
	public  String html2markdown(String xhtml) {
		String markdown="";		      
		log.trace("initial html:\n"+xhtml);
		//Markdown escaping
		xhtml = markdownEscaping(xhtml);
		Source xmlSource = new StreamSource(new StringReader(HTML_PREPEND+xhtml));
		try {
			
			Transformer trans = cachedXSLT.newTransformer();
			StringWriter strresult = new StringWriter();
			trans.transform(xmlSource, new StreamResult(strresult));
			System.out.println("XSLT "+strresult.getBuffer().toString());
			markdown=strresult.getBuffer().toString();

		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch (TransformerException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	
		log.trace("final markdown:\n"+markdown);
		return markdown;
	}

	/**
	 * This method transform Markdown document in XHTML data.
	 * @param markdown input document in Markdown
	 * @return representation of input document in XHTML
	 */
	public  String markdown2html(String markdown) {
		
		log.trace("initial markdown text:\n"+markdown);
		//escape the markdown not to interpret wrongly < or "
		markdown=LiteralEscaping(markdown);
		log.trace("initial markdown text escaped:\n"+markdown);		
		//Profile extended https://github.com/rjeschke/txtmark
		String html = Processor.process("[$PROFILE$]: extended\n"+markdown);
		//String html = Processor.process(markdown);
		//MarkdownProcessor m = new MarkdownProcessor(); 
		//String html = m.  markdown(markdown);
		log.trace("final html text:\n"+html);
		html = markdownUnescaping(html);
		log.trace("final html text escaped:\n"+html);
		return html;
	}
	
	/**
	 * ELCode to HTML translation. A subset of ELCode tag
	 * is transformed in the equivalent HTML tags to preserve
	 * old ELOG message formatting.
	 * ELCode spec: http://midas.psi.ch/elogs/Linux%20Demo/?cmd=HelpELCode
	 * 
	 * @param elcode
	 * @return
	 */
	public  String elcode2html(String elcode) {
		
		elcode = elcode.replaceAll("\\[b\\]", "<strong>");
		elcode = elcode.replaceAll("\\[/b\\]", "</strong>");
		elcode = elcode.replaceAll("\\[i\\]", "<em>");
		elcode = elcode.replaceAll("\\[/i\\]", "</em>");
		elcode = elcode.replaceAll("\\[h1\\]", "<h1>");
		elcode = elcode.replaceAll("\\[/h1\\]", "</h1>");
		elcode = elcode.replaceAll("\\[h2\\]", "<h2>");
		elcode = elcode.replaceAll("\\[/h2\\]", "</h2>");
		elcode = elcode.replaceAll("\\[h3\\]", "<h3>");
		elcode = elcode.replaceAll("\\[/h3\\]", "</h3>");
		elcode = elcode.replaceAll("\\[line\\]", "<hr/>");
		//Quotation
		elcode = elcode.replaceAll("\\[quote\\]", "<blockquote>");
		elcode = elcode.replaceAll("\\[/quote\\]", "</blockquote>");
		elcode = elcode.replaceAll("\\[quote=\".*\"\\]", "<blockquote>");
		elcode = elcode.replaceAll("\\[quote = \".*\"\\]", "<blockquote>");
		elcode = elcode.replaceAll("\\[/quote\\]", "</blockquote>");
		//Code
		elcode = elcode.replaceAll("\\[code\\]", "<pre><code>");
		elcode = elcode.replaceAll("\\[/code\\]", "</code></pre>");
		//List
		elcode = elcode.replaceAll("\\[list\\]", "<ul>");
		elcode = elcode.replaceAll("\\[/list\\]", "</ul>");
		elcode = elcode.replaceAll("\\[\\*\\]", "<li>");
		//No ordered or alphabetic list support
		elcode = elcode.replaceAll("\\[list=.*\\]", "<ul>");
		elcode = elcode.replaceAll("\\[list = .*\\]", "<ul>");
		
		
		
		//Not supported tags
		elcode = elcode.replaceAll("\\[color=[a-zA-Z0-9#]*\\]", "");
		elcode = elcode.replaceAll("\\[/color\\]", "");
		elcode = elcode.replaceAll("\\[font=[a-z]*\\]", "");
		elcode = elcode.replaceAll("\\[font = [a-z]*\\]", "");
		elcode = elcode.replaceAll("\\[/font\\]", "");
		elcode = elcode.replaceAll("\\[size=[0-9]*\\]", "");
		elcode = elcode.replaceAll("\\[size = [0-9]*\\]", "");
		elcode = elcode.replaceAll("\\[/size\\]", "");
		elcode = elcode.replaceAll("\\[center\\]", "");
		elcode = elcode.replaceAll("\\[/center\\]", "");
		elcode = elcode.replaceAll("\\[anchor\\]", "");
		elcode = elcode.replaceAll("\\[/anchor\\]", "");
		elcode = elcode.replaceAll("\\[table [a-zA-Z0-9\"=]*\\]", "");
		elcode = elcode.replaceAll("\\[table\\]", "");
		elcode = elcode.replaceAll("\\[/table\\]", "");
		
		return elcode;
		
		
		
	}
	/*
	 * 
	 */
	private  String LiteralEscaping(String input) {
		//Escape literal representation of HTML entities after database retrieval
		//so the browser does not interpret chars as html tags
		
		//this is commented out otherwise a link with request parameters is not interpreted correctly when clicked
		//input = input.replace("&", "&amp;");
		//this is commented out, otherwise it is not seen as <blockquote> tag
		//input = input.replace(">", "&gt;");
	    if(input!=null)
	        input = input.replace("<", "&lt;");
		return input;
	}

	private  String LiteralUnescaping(String input) {
		//unescape literal representation of HTML entities before putting into the database
		//so the email text is displayed correctly; useful for REST interface as well
		
		//Fix issue with tinyMice and amp char not encoded
		input = input.replace("&amp;", "&");
		input = input.replace("&gt;", ">");
		input = input.replace("&lt;", "<");

		return input;
	}
	
	private  String markdownEscaping(String input) {
		//Escaping markdown char to prevent interpretation
		input= input.replace("*", "\\*");
		input= input.replace("+", "\\+");
		input= input.replace("-", "\\-");
		input= input.replace("_", "\\_");
		//To avoid replacement of valid = chars such as in link or table (href="alink")
		input= input.replace("===", "\\=\\=\\=");
		return input;
	}
	
	public  String markdownUnescaping(String input) {
		//Escaping markdown char to prevent interpretation
		input= input.replace("\\*", "*");
		input= input.replace("\\+", "+");
		input= input.replace("\\-", "-");
		input= input.replace("\\_", "_");
		input= input.replace("\\=", "=");
		input= input.replace("&ldquo;", "\"");
		input= input.replace("&rdquo;", "\"");
		
		return input;
	}
	

	
	
}
