package elisa.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import java.net.InetAddress;
import java.net.UnknownHostException;

import elisa.rest.model.Attachment;
import elisa.rest.model.Option;

public class EmailNotificationImpl implements EmailNotification {
	private static final Log log = LogFactory.getLog(EmailNotificationImpl.class.getName());
    private final static String DEFAULT_LOGBOOK_NAME ="ProtoDUNE-DP";
	
	private MailSender mailSender;
    private SimpleMailMessage templateMessage;

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setTemplateMessage(SimpleMailMessage templateMessage) {
        this.templateMessage = templateMessage;
    }

    //MatchAddresses-algo taken from atlogd
    public List<String> MatchAddresses (String mail_to) {
        String[] addresses=mail_to.split(",");
        HashSet<String> send_to=new HashSet<String>();
        HashSet<String> do_not_send=new HashSet<String>();
        HashSet<String> explicitely_send=new HashSet<String>();
        List<String> send_to_list=new ArrayList<String>();

        // address -> how many times
        HashMap<String,Integer> repeated=new HashMap<String,Integer>();
 
        for (int i=0; i< addresses.length;i++)
        { 
        	if (addresses[i].contains("NONE")) {
                send_to_list.clear();
                log.info("MAIL_IN: "+mail_to);
                log.info("MAIL_OUT: NONE");
                return send_to_list;
            }
            if (repeated.containsKey(addresses[i])) { 
                // get number of occurrences for this address
                // increment it  
                // and put back again  
                repeated.put(addresses[i], repeated.get(addresses[i]) + 1);
            } else { 
                // this is first time we see this address, set value '1' 
                repeated.put(addresses[i], 1);
            } 
        }
        
        for (java.util.Map.Entry<String, Integer> item : repeated.entrySet()) {
        	  String key = item.getKey();
        	  int times = item.getValue();
        	  log.trace("Key: " + key + ", Value: " + times);
        	  String[] address = key.split("/");
        	  switch (address.length) {
        	  case 0:
                //log.debug("not to send");
                break;
        	  case 1:
        		//log.debug("case 1, put to send_to"); 
                send_to.add (address[0].trim());
                break;
        	  default:
                String requested_times =  address[address.length - 1];
                //log.debug("requested_times "+requested_times); 
                if (requested_times.length() != 1) {
                	//log.debug("bad digit: "+requested_times);
                    continue;
                }
                if (requested_times.equals("0")) {
                	log.debug("do_not_send"+address[0] );
                    do_not_send.add (address[0].trim());
                } 
                else if (requested_times.equals("2") && times>=Integer.parseInt(requested_times)) {
                	log.debug("send_to"+address[0] );
                	send_to.add (address[0].trim());
                }
                else if (requested_times.equals("1")) {
                	log.debug("explicitely_send"+address[0] );
                	explicitely_send.add (address[0].trim());
                }
            }
        }
        for(Iterator<String> it=do_not_send.iterator();it.hasNext();)  {
        	String elem=it.next();
        	log.debug("elem to remove "+elem);
        	send_to.remove(elem);
        }
        for(Iterator<String> it=explicitely_send.iterator();it.hasNext();)  {
        	String elem=it.next();
        	log.debug("elem to add explicitely "+elem);
        	send_to.add(elem);
        }
       
        for(Iterator<String> it=send_to.iterator();it.hasNext();) {
        	String em=it.next();
        	//log.debug("final recipient:"+em+"!!");
        	if(em.isEmpty()==false && em.equals(" ")==false){
        	send_to_list.add(em);
        	}
        }
        log.info("MAIL_IN: "+mail_to);
        String mail_out="";
        for(Iterator<String> it=send_to_list.iterator();it.hasNext();) {
        	mail_out+=it.next()+", ";
        }
        log.info("MAIL_OUT: "+mail_out);
        
        return send_to_list;
    }

    public String simplifyURL(String text){    		    	
    	    //find and remove the first part [] of the URL in markdown [http:](http:)
    	    Pattern p = Pattern.compile("(\\[)(http[s]?://[a-zA-Z0-9+&@#/%?=~_|!:.;\\\\-]+)(\\])");
    		Matcher m = p.matcher(text);
    		StringBuffer sb = new StringBuffer();
    		boolean result = m.find();  		
    		while(result) {
        	  m.appendReplacement(sb, "");
        	  result=m.find();
    		}  
    		// Add the last segment of input to the new String
    		m.appendTail(sb);
    		//log.debug(sb.toString());
    		return sb.toString();
    }
    
	public void sendNotification(Email em, boolean sendit) {
        // Create a thread safe "copy" of the template message and customize it
        log.debug("logbook "+em.getLogbookName());
		SimpleMailMessage msg = new SimpleMailMessage(this.templateMessage);
        final List<String> ToRecipients=MatchAddresses(em.getToRecipients());
        String hostname=null;
        if(ToRecipients.size()!=0){
		try{
			InetAddress addr = InetAddress.getLocalHost();
			String serverip = addr.getHostAddress();
			hostname = addr.getHostName();
			log.debug("hostname="+hostname);
			log.debug("server hostip="+serverip);
			String[] ToRecipientsArray = ToRecipients.toArray(new String[ToRecipients.size()]);
			if(hostname.contains("vmk-atd-elisa-2")){
				String[] recip= new String[] { "alina.radu@cern.ch"};
				msg.setBcc(recip);
				log.info("Test instance - email sent to: alina.radu@cern.ch");				
			}
			else {
				msg.setBcc(ToRecipientsArray);
			}
			//use real recipients if server is in P1 or if server is ftkelisa or atlasdqlog, else use it for test.
			/*if(serverip.startsWith("10.")){
			   msg.setBcc(ToRecipientsArray);
			}
			else if (hostname.contains("ftkelisa"))
				{
					msg.setBcc(ToRecipientsArray);
				}
			else if (hostname.contains("aiatlas059") || hostname.contains("atlasdqlog")) //or its alias for DQshift
				{
					msg.setBcc(ToRecipientsArray);
				}
			/*else if (hostname.contains("vmk-atd-elisa-2"))
			{
				String[] recip = new String[] { "alina.radu@cern.ch"};
				msg.setBcc(recip);
				log.info("Test instance - email sent to: alina.radu@cern.ch");
			}*/
			/*else {
			   String[] recip= new String[] { "alina.radu@cern.ch"};
			   msg.setBcc(recip);
			   log.info("Test instance - email sent to: alina.radu@cern.ch");
			}*/
		    } catch (UnknownHostException e) {
			  log.error("can't get server host name");
		    }

		msg.setReplyTo("pddp-elog@cern.ch");

		String subject="";
        if(em.getAuthor()!=null){
        	subject=subject.concat("[").concat(em.getAuthor()).concat("]");
        }
        if(em.getMessageType()!=null){
        	if(em.getMessageType().equals("Data Quality")){
            	subject=subject.concat("[").concat(em.getMessageType()).concat("]");
                for(Option o:em.getOptions().getList()) {
  				  if(o.getName().equals("DQ_Type")){
  					  subject=subject.concat("[").concat(o.getValue()).concat("] ");
  				  }
    			}        		
        	}
        	else {
            	subject=subject.concat("[").concat(em.getMessageType()).concat("] ");        		
        	}
        }
        if (em.getLogbookName().equals("FTK")){
        	subject = subject.concat("[LAB4] ");
        }
        else if (em.getLogbookName().equals("TEST")){
        	subject = subject.concat("[TEST] ");
        }
        else if (em.getLogbookName().equals("DQshift")){
        	subject = subject.concat("[DQShift] ");
        }
        
        if(em.getSubject()!=null){
        	subject=subject.concat(em.getSubject());
        }
        		
        msg.setSubject(subject);
   
        String body=em.getBody();  
        String mail_link = new String("");
       	mail_link = "https://pddpelog.web.cern.ch/elisa/";

        
        //log.debug("email text in markdown:"+body);
        //Simplify the url to be displayed in emails.
        String body_simplify_url=simplifyURL(body);
        log.debug("body_simplify_url: "+body_simplify_url);
        String text=
        	"------- WRITE YOUR REPLY ABOVE ------\r\n"+
        	"Author:            "+em.getAuthor()+"\r\n"+
        	"Status:            "+em.getStatus()+"\r\n"+
        	"System Affected:   "+em.getSystemAffected()+"\r\n"+
        	"Message type:      "+em.getMessageType()+"\r\n";
            if(em.getOptions()!=null){
            for(Option o:em.getOptions().getList()) {
            	text+=
                o.getName()+":      "+o.getValue()+"\r\n";
			}
            }
            if(em.getLogbookName().equals(DEFAULT_LOGBOOK_NAME)){
            	text+=
            	"Display:           " +mail_link+"display/"+em.getMessageID()+"\r\n"+
            	"Reply to:          " +mail_link+"replyTo/" +em.getMessageID()+"\r\n"+
            	"Edit:              " +mail_link+"edit/"+em.getMessageID()+"\r\n";
            	if(em.getAttachments()!=null){
            		text+="Attachments:       ";	
            		for(Iterator<Attachment> it=em.getAttachments().getList().iterator();it.hasNext();){
            			Attachment a=(Attachment)it.next();
            			text+=mail_link+"attachment?name="+a.getFileName()+"&fileID="+a.getID()+" \r\n";        			
            			if(it.hasNext()==true) text+="                   ";
            		}
            	}
            }
            else {
                text+=
            	"Display:           " +mail_link+"display/"+em.getMessageID()+"?logbook="+em.getLogbookName()+"\r\n"+
            	"Reply to:          " +mail_link+"replyTo/" +em.getMessageID()+"?logbook="+em.getLogbookName()+"\r\n"+
            	"Edit:              " +mail_link+"edit/"+em.getMessageID()+"?logbook="+em.getLogbookName()+"\r\n";
                if(em.getAttachments()!=null){
                   	text+="Attachments:       ";	
                   	for(Iterator<Attachment> it=em.getAttachments().getList().iterator();it.hasNext();){
                   		Attachment a=(Attachment)it.next();
                   		text+=mail_link+"attachment?name="+a.getFileName()+"&fileID="+a.getID()+"?logbook="+em.getLogbookName()+" \r\n";        			
                	    if(it.hasNext()==true) text+="                   ";
                    }
                }            	
            }
        text+="----------------------------------\r\n"+
			body_simplify_url+"\r\n"+
        	"----------------------------------\n"+
        	"This is an automatically generated email based on the ELisA (ATLAS electronic logbook) software.";
        
        msg.setText(text);
        try{
        	if(sendit==true)  {          
        		this.mailSender.send(msg);
        		log.debug("replyTo: "+msg.getReplyTo());
        	}
        	else {
        		log.info("Email NOT sent to recipients");
        	}
        }
        catch(MailException ex) {
            // simply log it and go on...
           log.error(ex.getMessage());            
        }
        }
        else log.info("No sender specified.");

	}

}
