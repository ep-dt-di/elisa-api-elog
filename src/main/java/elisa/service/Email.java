package elisa.service;

import java.util.Set;

import elisa.rest.model.AttachmentList;
import elisa.rest.model.OptionList;

public class Email {
	private long messageID;
	private String author;
	private String messageType;
	private String systemAffected;
	private String status;
	private String subject;
	private String body;
	private String toRecipients;
	private String logbook;
	//private List<Attachments> attachmentData;
	private AttachmentList attachments ;
	private OptionList options;
	
	public void setMessageID(long messageID) {
		this.messageID = messageID;
	}
	public long getMessageID() {
		return messageID;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getAuthor() {
		return author;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setSystemAffected(String systemAffected) {
		this.systemAffected = systemAffected;
	}
	public String getSystemAffected() {
		return systemAffected;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSubject() {
		return subject;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getBody() {
		return body;
	}
	public void setToRecipients(String toRecipients) {
		this.toRecipients = toRecipients;
	}
	public String getToRecipients() {
		return toRecipients;
	}	
	public AttachmentList getAttachments() {
	    return this.attachments;
	}
	public void setAttachmentList(AttachmentList list) {
	    if (attachments==null)
	            this.attachments= new AttachmentList();
	    this.attachments = list;
	}
	public OptionList getOptions() {
		return options;
	}
	public void setOptions(OptionList options) {
		this.options = options;
	}
	public String getLogbookName() {
		return logbook;
	}
	public void setLogbookName(String logbook) {
		this.logbook = logbook;
	}

}
