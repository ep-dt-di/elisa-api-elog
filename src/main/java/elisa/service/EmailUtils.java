package elisa.service;

import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import elisa.rest.ConfigurationDAO;
import elisa.rest.model.Message;
import elisa.rest.model.Option;

public class EmailUtils {
    
    private static final Log log = LogFactory.getLog(EmailUtils.class);
    
    @Autowired
    private  ConfigurationDAO configurationDAO;
    @Autowired 
    private  Utils utils;
    @Autowired
    private  EmailNotification emailNotification;
    
    /**
     * Create the email notification for the message in input.
     * 
     * @param entry
     */
    public  void createEmail(Message entry){
        Email email_obj=new Email();
        
        //get emails for MT specific, SA specific and for MT-Option specific
        log.debug("mt "+entry.getMessageType());
        String MTemailRecipients=configurationDAO.getEmailrecipientsByMTName(entry.getMessageType());
        log.debug("getEmailrecipientsByMTName " +MTemailRecipients);
        String SAemailRecipients="";
        String presetEmailRecipients="";
        String sa=entry.getSystemAffectedAsString();
        log.debug("sa "+sa);
        String[] eachSA = sa.split (",");
        for (int i=0; i < eachSA.length; i++){
             eachSA[i]=eachSA[i].trim();
             String SAemail=configurationDAO.getEmailrecipientsBySAName(eachSA[i]);
             log.debug("SAName " +eachSA[i]+" getEmailrecipientsBySAName " +SAemail);            
             if(SAemail!=null){
                 SAemailRecipients=SAemailRecipients.concat(SAemail).concat(", ");}
        }
        
        if(entry.getOptionalAttributes()!=null){
        for(Option o:entry.getOptionalAttributes().getList()) {
            System.out.println("o.getName() "+o.getName()+" o.getValue() "+o.getValue());
            presetEmailRecipients=presetEmailRecipients.concat(configurationDAO.getPresetEmailrecipients(o.getName(),o.getValue(),entry.getMessageType()));
            presetEmailRecipients=presetEmailRecipients.concat(", ");                        
        }
        }
        log.debug("presetEmailRecipients " +presetEmailRecipients);     
        String allrecip="";
        if(MTemailRecipients!=null) allrecip=SAemailRecipients.concat(MTemailRecipients); else allrecip=SAemailRecipients;
        if(!presetEmailRecipients.isEmpty()) allrecip=allrecip.concat(", ").concat(presetEmailRecipients);
        
        email_obj.setToRecipients(allrecip);
        email_obj.setMessageID(entry.getID());
        email_obj.setAuthor(entry.getAuthor());
        email_obj.setMessageType(entry.getMessageType());
        email_obj.setSubject(entry.getSubject());
        email_obj.setSystemAffected(entry.getSystemAffectedAsString());
        email_obj.setStatus(entry.getStatus()); 
        email_obj.setBody(utils.markdownUnescaping(entry.getBody()));
        if(entry.getHasAttachments()==1&&entry.getAttachments()!=null)
            email_obj.setAttachmentList(entry.getAttachments());    
        email_obj.setOptions(entry.getOptionalAttributes());
        email_obj.setLogbookName(configurationDAO.getLogbookName(entry.getLogbook()));
        //return email_obj;
        
        emailNotification.sendNotification(email_obj, true);        
    }


}
