package elisa.service;

import javax.naming.*;
import javax.naming.directory.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Hashtable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.userdetails.*;

/**
 * Code form ElisA service package
 *
 */
public class LdapP1UserQuery extends InetOrgPersonContextMapper {

    private static final Log log = LogFactory.getLog(LdapP1UserQuery.class);

    @Override
    public UserDetails mapUserFromContext(DirContextOperations ctx,String username, Collection<? extends GrantedAuthority> authorities) 
            throws UsernameNotFoundException {

        String serverip = null;
        try{
            InetAddress addr = InetAddress.getLocalHost();
            serverip = addr.getHostAddress();
            log.trace("Server hostip="+serverip);
        } catch (UnknownHostException e) {
            log.error("Can't get server host name");
        }
        String query;
        if(serverip.startsWith("10.")){
            //P1
            query="uid=" + username;
        }
        else{
            //GPN
            query="cn=" + username;
        }
        String attribute="sn";
        StringBuffer output = new StringBuffer();

        try {
            String url;
            if(serverip.startsWith("10.")){
                //P1
                url = "ldap://atlas-ldap.cern.ch/ou=people,ou=atlas,o=cern,c=ch";
            }
            else {
                //GPN
                url = "ldap://xldap.cern.ch/ou=users,ou=organic%20units,dc=cern,dc=ch";
            }
            Hashtable env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            //env.put(Context.INITIAL_CONTEXT_FACTORY, "org.");
            env.put(Context.PROVIDER_URL, url);
            //set the connection timeout to 5sec 
            env.put("com.sun.jndi.ldap.connect.timeout", "5000");
            DirContext newContext = new InitialDirContext(env);

            SearchControls ctrl = new SearchControls();
            ctrl.setSearchScope(SearchControls.SUBTREE_SCOPE);
            log.trace("LDAP filter: "+query);
            //NamingEnumeration results = newContext.search(searchBase, searchFilter, ctrl);
            NamingEnumeration enumeration = newContext.search("", query, ctrl);
            while (enumeration.hasMore()) {
                SearchResult result = (SearchResult) enumeration.next();
                Attributes attribs = result.getAttributes();
                NamingEnumeration values = ((BasicAttribute) attribs.get(attribute)).getAll();
                while (values.hasMore()) {
                    if (output.length() > 0) {
                        output.append("|");
                    }
                    output.append(values.next().toString());
                }
            }

            /*while (results != null && results.hasMoreElements()) {
    		System.out.println(">>> here….");
    		SearchResult sr = (SearchResult)results.next();
    		//print out the name
    		System.out.println("name: " + sr.getName());
    		}*/
        } catch (Exception e) {
            e.printStackTrace();
            throw new UsernameNotFoundException("Invalid login credentials");
        }
        if(output.length()==0){
            throw new UsernameNotFoundException("Invalid login credentials");
        } else {

            log.debug("login "+output.toString());
            return super.mapUserFromContext(ctx, username, authorities);
        }
    }

}
