package elisa.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class SSOUserDetailsService implements AuthenticationUserDetailsService{
	private UserDetailsService userService;
	
	public UserDetailsService getUserService() {
		return userService;
	}

	public void setUserService(UserDetailsService userService) {
		this.userService = userService;
	}
	
	@Override
	public UserDetails loadUserDetails(Authentication a) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
	    String username = (String) a.getPrincipal();

	    //System.out.println("SSOUserDetails get credentials "+ a.getCredentials().toString());
        System.out.println("SSOUserDetails get username:"+username);
        
//UserDetails user = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
//        user.isEnabled(), user.isAccountNonExpired(), user.isCredentialsNonExpired(),
//        user.isAccountNonLocked(), user.getAuthorities()) ;
UserDetails user = new org.springframework.security.core.userdetails.User(username, a.getCredentials().toString(),
		true,true,true,true,a.getAuthorities());
	   
	return user;
	}
}
