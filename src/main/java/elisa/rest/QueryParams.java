package elisa.rest;

public class QueryParams {
    
    //Filtering Criteria
	private final String author;
	private final String username;
	private final String systemAffected;
	private final String startDate;
	private final String endDate;
	private final String messageType;
	private final String subject;
	private final Integer numberOfMonthsToQuery;
	private final String text;
    private final Integer logbookID;
    private final String runNumber;
	
	//Pagination Criteria
	private final Long lastID;
	private final Long firstID;
    private final Integer numberOfMessagesToReturn;
    private final Integer page;

	public static class Builder {
		
		private String author = null;
		private String username = null;
		private String systemAffected = null;;
		private String startDate = null;
		private String endDate = null;
		private String messageType = null;
		private String subject = null;
		private Integer numberOfMonthsToQuery = null;
	    private String text=null;
		private Long lastID = null;
		private Long firstID = null;
		private Integer numberOfMessagesToReturn;
	    private Integer page= null;
	    private Integer logbookID=null;
	    private String runNumber=null;
		
		public Builder author(String s) {
			author = s; return this;
		}
		public Builder username(String s){
			username = s; return this;
		}
		public Builder systemAffected(String s){
			systemAffected = s; return this;
		}
		public Builder startDate(String s){
			startDate = s; return this;
		}
		public Builder endDate(String s){
			endDate = s; return this;
		}
		public Builder messageType(String s){
			messageType = s; return this;
		}
		public Builder subject(String s){
			subject = s; return this;
		}
		public Builder numberOfMonthsToQuery(Integer m) {
		    numberOfMonthsToQuery = m; return this;
		}
		public Builder text(String s){
            text=s; return this;
        }
		
		public Builder lastID(Long l){
            lastID=l; return this;
        }
		public Builder firstID(Long l){
            firstID=l; return this;
        }
		public Builder numberOfMessagesToReturn(Integer l){
            numberOfMessagesToReturn=l; return this;
        }
		public Builder page(Integer n){
            page=n; return this;
        }

		public Builder logbookID(Integer l){
            logbookID=l; return this;
        }

		public Builder runNumber(String r){
            runNumber=r; return this;
        }
		
		public QueryParams build() {
			return new QueryParams(this);
		}
	}
	
	private QueryParams(Builder builder) {
		author = builder.author;
		username = builder.username;
		systemAffected = builder.systemAffected;
		startDate = builder.startDate;
		endDate = builder.endDate;
		messageType = builder.messageType;
		subject = builder.subject;
		numberOfMonthsToQuery = builder.numberOfMonthsToQuery;
		text = builder.text;
		logbookID = builder.logbookID;
		runNumber=builder.runNumber;
		
		firstID=builder.firstID;
		lastID=builder.lastID;
		numberOfMessagesToReturn=builder.numberOfMessagesToReturn;
		page=builder.page;
		
	}
	
	public Boolean isEmpty() {
		if ((author==null)&&
		(username==null)&&
		(systemAffected==null)&&
		(startDate==null)&&
		(endDate==null)&&
		(messageType==null)&&
		(subject==null) &&
		(numberOfMonthsToQuery==null) &&
		(numberOfMessagesToReturn==null)&&
		(text==null)&&
		(runNumber==null)&&
		(page==null)
        )
			return true;
		else
			return false;
	}
	
	
	public Boolean hasFilterCriteria() {
		if ((author==null)&&
		(username==null)&&
		(systemAffected==null)&&
		(messageType==null)&&
		(subject==null)&&
		(text==null)&&
		(runNumber==null)&&
		(startDate==null)&&
		(endDate==null)
		)
			return false;
		else
			return true;
	}
	
	public String getAuthor() {
		return author;
	}

	public String getUsername() {
		return username;
	}

	public String getSystemAffected() {
		return systemAffected;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getMessageType() {
		return messageType;
	}

	public String getSubject() {
		return subject;
	}
	
	public Integer getNumberOfMonthsToQuery() {
	    return numberOfMonthsToQuery;
	}

	public String getText() {
		return text;
	}
	
	public Long getLastID() {
        return lastID;
    }
	public Long getFirstID() {
        return firstID;
    }
	public Integer getNumberOfMessagesToReturn() {
        return numberOfMessagesToReturn;
    }
	public Integer getPage() {
        return page;
    }
	public Integer getLogbookID() {
		return logbookID;
	}

	public String getRunNumber() {
		return runNumber;
	}
	
	
}
