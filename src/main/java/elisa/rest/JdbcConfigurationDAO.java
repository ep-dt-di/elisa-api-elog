package elisa.rest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import elisa.rest.model.OptionMetadata;


public class JdbcConfigurationDAO implements ConfigurationDAO {

    private static final Log log = LogFactory.getLog(JdbcConfigurationDAO.class.getName());

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    public JdbcConfigurationDAO(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Retrieve the list of message types per logbook
     * @return List<String> each entry represents a message type 
     * 
     */
    @Override
    public List<String> getMessageTypes(int logbookID) {
        log.debug("Retrieving from DB message types for logbook "+getLogbookName(logbookID));
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        final String sql = "SELECT MT_NAME from LOGBOOK2MT_VIEW WHERE LOGBOOKNAME=:LOGNAME ORDER BY MT_ID";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource().addValue("LOGNAME", getLogbookName(logbookID));
		return jdbcTemplate.queryForList(sql,namedParameters,java.lang.String.class);
    }

    /**
     * Retrieve the list of message types
     * @return List<String> each entry represents a message type 
     * 
     */
    @Override
    public List<String> getMessageTypes() {
        log.trace("Retrieving from DB the list of possible message types for default ATLAS logbook (ID=70)");
        return this.getMessageTypes(70);
    }

    /**
     * Get the list of system affected per message type
     * @param List<String> list of system affected for the given message type
     */
    @Override
    public List<String> getSystemAffected(String messageType) {

        log.trace("Retrieving from DB system affected list per message type: "+messageType);
        return jdbcTemplate.queryForList("SELECT SA_NAME FROM MT2SA_VIEW WHERE MT_NAME= ? ORDER BY SA_ID", 
                new Object[]{messageType},
                String.class);
    }
    /**
     * Retrieve list of optional attributes metadata for the specified
     * message type from EliSA configuration.
     * <p/>
     * The method returns both first level and inner levels attributes metadata, 
     * as defined by configuration
     * 
     * @param the message type
     * @return List<OptionMetaData>
     */
    @Override
    public List<OptionMetadata> getOptionsMetadata(String messageType) {
        log.trace("Retrieving from DB the list of additional attributes metadata per message type: "+messageType);
        List<OptionMetadata> options = namedJdbcTemplate.query("SELECT OPTIONS.OPT_NAME, OPTIONS.OPT_VALUES, OPTIONS.OPT_TYPE, OPTIONS.ID " +
                "FROM OPTIONS JOIN MT2OPTIONS_VIEW ON MT2OPTIONS_VIEW.OPT_ID = OPTIONS.ID "+
                "WHERE MT2OPTIONS_VIEW.MT_NAME=:mt_name", 
                new MapSqlParameterSource().addValue("mt_name", messageType),
                new OptionMetadataMapper());

        for(OptionMetadata option: options) {
            List<OptionMetadata> inneropts = this.getInnerOptionsMetadata(option.getID());
            if(!inneropts.isEmpty())
                option.setChildOptions(inneropts);
        }
        return options;

    }
    
    /**
     * Retrieve the optional attribute metadata for a 
     * option identified by the name and the message type
     * @param String messageType the message type the option belong
     * @param String optionName the name of the option to be retrieved
     * 
     * @return OptionMetadata attributes metadata
     */

    @Override
    public OptionMetadata getOptionMetadata(String messageType, String optionName) {
        log.trace("Retrieving from DB the metadata for the attribute: "+optionName+" for the per message type: "+messageType);
        //Option op = jdbcTemplate.query(sql, namedParameters, new OptionMapper());
        OptionMetadata op = namedJdbcTemplate.queryForObject( "SELECT OPTIONS.OPT_NAME, OPTIONS.OPT_VALUES, OPTIONS.OPT_TYPE, OPTIONS.ID " +
                "FROM OPTIONS JOIN MT2OPTIONS_VIEW ON MT2OPTIONS_VIEW.OPT_ID = OPTIONS.ID "+
                "WHERE MT2OPTIONS_VIEW.MT_NAME=:mt_name AND OPTIONS.OPT_NAME=:opt_name",
                new MapSqlParameterSource().addValue("mt_name", messageType).addValue("opt_name", optionName), 
                new OptionMetadataMapper());

        op.setChildOptions(this.getInnerOptionsMetadata(op.getID()));
        return op;
    }
    

    /**
     * Retrieve the list of attribute metadata for a inner option by ID
     *  
     * @param ID
     * @return List<OptionMetadata> list of options metadata
     */
    private List<OptionMetadata> getInnerOptionsMetadata(Long ID) {
        return namedJdbcTemplate.query( "SELECT OPTIONS.OPT_NAME, OPTIONS.OPT_VALUES, OPTIONS.OPT_TYPE, OPTIONS.ID " +
                    "FROM OPTIONS WHERE OPTIONS.OPTION_PARENT_ID = :opt_id AND OPTIONS.OPT_TYPE != 'preset'", 
                    new MapSqlParameterSource().addValue("opt_id", ID), 
                    new OptionMetadataMapper());

    }


    /**
     * Retrieve the optional attribute metadata for a 
     * second level option identified by the name, the value of the 
     * parent option and the message type
     * @param String messageType the message type the option belong
     * @param String optionValue
     * @param String optionName the name of the option to be retrieved
     * 
     * @return OptionMetadata attributes metadata
     */

    @Override
    public List<OptionMetadata> getSecondLevelOptionMetadata(String messageType, String parentOptionName, String parentOptionValue) {
        log.debug("Retrieving from DB the metadata for the inner attribute: "+parentOptionName+", parent value: "+parentOptionValue+", " +
                " for the per message type: "+messageType);
        
        return namedJdbcTemplate.query(
                "SELECT OPTIONS.OPT_NAME, OPTIONS.OPT_VALUES, OPTIONS.OPT_TYPE, OPTIONS.ID " +
                        "FROM OPTIONS WHERE OPTIONS.OPTION_PARENT_ID IN " +
                        "(SELECT OPTIONS.ID FROM OPTIONS JOIN MT2OPTIONS_VIEW ON MT2OPTIONS_VIEW.OPT_ID = OPTIONS.ID "+ 
                        " WHERE MT2OPTIONS_VIEW.MT_NAME=:mt_name AND OPTIONS.OPT_NAME=:opt_name) "+
                        " AND OPTIONS.OPTION_PARENT_NAME=:opt_value", 
                        new MapSqlParameterSource().addValue("mt_name", messageType).
                        addValue("opt_name", parentOptionName).
                        addValue("opt_value", parentOptionValue), 
                        new OptionMetadataMapper());
    }

    @Override
    public List<OptionMetadata> getSecondLevelOptionsMetadata(String messageType,
            String optionName) {


        String sql=
                "SELECT OPTIONS.OPT_NAME, OPTIONS.OPT_VALUES, OPTIONS.OPT_TYPE, OPTIONS.ID " +
                        "FROM OPTIONS WHERE OPTIONS.OPT_LEVEL != 'L3' and OPTIONS.OPTION_PARENT_ID IN" +
                        " (SELECT OPTIONS.ID FROM OPTIONS WHERE upper(OPTIONS.OPT_NAME)=:optName AND OPTIONS.OPTION_PARENT_NAME=:messType)";

        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

        
        MapSqlParameterSource namedParameters = new MapSqlParameterSource("messType", messageType).addValue("optName", optionName);
        
        log.debug("Looking for inner option for option name: "+optionName+", message type: "+messageType);

        try{
            List<OptionMetadata> result = jdbcTemplate.query(sql, namedParameters, new OptionMetadataMapper());
            log.debug("how many inner options:"+result.size());
            return result;

        }catch(EmptyResultDataAccessException erDAE){
            log.error(erDAE.getMessage());
            throw erDAE;
        }catch(DataAccessException eDAE){
            log.error(eDAE.getMessage());           
            throw eDAE;
        }
    }

    /**
     * 
     */
    @Override
    public List<String> getSystemAffected(int logbookID) {
        log.debug("Retrieving from DB the list of possible system affected.");
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        //return jdbcTemplate.queryForList( "SELECT SA_NAME FROM SYSTEM_AFFECTED",String.class);
		final String sql = "select SA_NAME from LOGBOOK2SA_VIEW WHERE LOGBOOKNAME=:LOGNAME ORDER BY SA_ID ";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource().addValue("LOGNAME", getLogbookName(logbookID));
		List<String> sas = jdbcTemplate.queryForList(sql,namedParameters,java.lang.String.class);

		/*List<String> sas = jdbcTemplate.queryForList( "SELECT SA_NAME FROM SYSTEM_AFFECTED",String.class);
        List<String> sa_specific = new ArrayList<String>();
        if(getLogbookName(logbookID).equals("FTK")){
			sa_specific = Arrays.asList(
					"FTK",
					"IM",
					"DF",
					"AUX",
					"AMB",
					"SSB",
					"FLIC",
					"AUX",
					"FTK-DCS",
					"FTK-Infrastructure");
			sas.retainAll(sa_specific);
		}
		else if (getLogbookName(logbookID).equals("DQshift")) {
			sa_specific = Arrays.asList(
		    		"Run",
		    		"Processing_Pass",
					"Pixel",
		    		"SCT",
		    		"Muons",
		    		"TRT",
		    		"LAr",
		    		"CaloGlobal",
		    		"EGamma",
		    		"Tau",
		    		"ID",
		    		"BTag",
		    		"Tile",
		    		"JetEtmiss",
		    		"MCP",
		    		"Luminosity",
		    		"L1Calo",
		    		"HLT",
		    		"Global",
		    		"CaloCombined");
			sas.retainAll(sa_specific);
		}
		else {
			sa_specific = Arrays.asList(
		    		"Pixel",
		    		"SCT",
		    		"TRT",
		    		"ID Gen. (IC)",
		    		"BCM",
		    		"Beam Conditions",
		    		"LAr",
		    		"Tile",
		    		"Lucid",
		    		"ZDC",
		    		"ALFA (RPO)",
		    		"AFP",
		    		"MDT",
		    		"RPC",
		    		"TGC",
		    		"CSC",
		    		"MMG",
		    		"DAQ",
		    		"Central DAQ",
		    		"HLT",
		    		"LVL1",
		    		"FTK",
		    		"Monitoring",
		    		"DataQuality",
		    		"Event Displays",
		    		"Network",
		    		"SysAdmins",
		    		"Magnets",
		    		"Cryo",
		    		"DCS",
		    		"Central DCS",
		    		"DSS",
		    		"Counting Room",
		    		"GAS",
		    		"Radioprotection",
		    		"Tech. Infra",
		    		"Safety",
		    		"Tier0",
		    		"RunCoord Info",
		    		"OnlineDB",
		    		"Other");
			sas.retainAll(sa_specific);	
		}*/        
        return sas;
    }
    @Override
    public List<String> getSystemAffected() {
        log.trace("Retrieving from DB the list of possible system affected for default ATLAS logbook (ID=70)");
        return this.getSystemAffected(70);
    }
    
    
	/*
	 * get String with comma separated emails per MT
	 */
	public String getEmailrecipientsByMTName(String mt_type) {
		String sql =
			  "SELECT EMAIL FROM MESSAGE_TYPE "+
			  "WHERE MT_NAME=:MT_NAME ";
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		MapSqlParameterSource namedParameters = new MapSqlParameterSource("MT_NAME", mt_type);
		try{
			String email_recipients = jdbcTemplate.queryForObject(sql,namedParameters,java.lang.String.class);
			return email_recipients;
		}catch(EmptyResultDataAccessException erDAE){
			return "";
		}catch(DataAccessException eDAE){
			throw eDAE;
		}
	}	

	/*
	 * get a String with comma separated list of emails per SA name
	 */
	public String getEmailrecipientsBySAName(String sa_name) {
		//System.out.println("SA "+sa_name);
		String sql =  
			 "SELECT EMAIL FROM SYSTEM_AFFECTED "+
			  "WHERE SA_NAME=:SA_NAME ";
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

		SqlParameterSource namedParameters = new MapSqlParameterSource("SA_NAME",sa_name );
		try{
			String email_SA = jdbcTemplate.queryForObject(sql,namedParameters,java.lang.String.class);
			return email_SA;
		}catch(EmptyResultDataAccessException erDAE){
			return "";
		}catch(DataAccessException eDAE){
			throw eDAE;
		}
	}			

	/*
	 * get a String with comma separated list of emails per preset option
	 */
	public String getPresetEmailrecipients(String option_name, String option_value, String mt_name) {
		String presetEmail;
		String sql =
			"SELECT PRESET_EMAIL from OPTIONS,PRESET_OPTION "+
			"where  OPT_LEVEL='L3' "+
			"AND ((OPTION_PARENT_ID IN (SELECt ID from OPTIONS "+
			"      WHERE (OPT_LEVEL='L2' AND OPTION_PARENT_ID IN (SELECT OPT_ID FROM MT2OPTIONS_VIEW WHERE MT_NAME=:MT_NAME )))) "+
			"OR "+
			" (OPTION_PARENT_ID IN (SELECT OPT_ID FROM MT2OPTIONS_VIEW WHERE MT_NAME=:MT_NAME ))) "+
			"AND ID=OPTION_ID "+
			"AND OPTION_NAME=:OPTION_NAME ";
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
				
		MapSqlParameterSource namedParameters = new MapSqlParameterSource()
			.addValue("OPTION_NAME", option_value)
			.addValue("MT_NAME", mt_name);
			
		try{
		presetEmail = jdbcTemplate.queryForObject(sql,namedParameters,java.lang.String.class);
		if (presetEmail==null) {presetEmail ="";}
		}catch(EmptyResultDataAccessException erDAE){
			return "";
		}catch(DataAccessException eDAE){
			throw eDAE;
		}
		return presetEmail;
	}			
	
	
	/**
	 * OptionMetadata mapper
	 * <p/>
	 * Expected value and order: OPTIONS.OPT_NAME, OPTIONS.OPT_VALUES, OPTIONS.OPT_TYPE, OPTIONS.ID
	 * @author lucamag
	 *
	 */
    private static class OptionMetadataMapper implements RowMapper<OptionMetadata> {

        public OptionMetadata mapRow(ResultSet rs, int rowNum) throws SQLException {
            OptionMetadata option = new OptionMetadata();
            option.setName(rs.getString(1));               //Option name
            option.setPossibleValues(rs.getString(2));              //Value
            option.setType(rs.getString(3));
            option.setID(rs.getLong(4));
            return option;
        }
    }

    @Override
	public int getLogbookID(String logbookname) {
		String sql = "select logbookid from LOGBOOK_CONFIG where LOGBOOKNAME = :LOGBOOKNAME ";
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		MapSqlParameterSource namedParameters = new MapSqlParameterSource("LOGBOOKNAME", logbookname);
		try {
			int logid = jdbcTemplate.queryForObject(sql,namedParameters,java.lang.Integer.class);
			return logid;
	    }catch(EmptyResultDataAccessException erDAE){
	    	log.error("No such logbook defined in the db.");
	    	throw new RuntimeException("ERROR: there is no such a logbook defined in the database.");
	    }catch(DataAccessException eDAE){
	    	throw eDAE;
	    }
	}

    @Override
	public String getLogbookName(int logbookid) {
    	//log.info("logbookid is "+logbookid);
		String sql = "select logbookname from LOGBOOK_CONFIG where LOGBOOKID = :LOGBOOKID ";
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		MapSqlParameterSource namedParameters = new MapSqlParameterSource("LOGBOOKID", logbookid);
		try {
			String logname = jdbcTemplate.queryForObject(sql,namedParameters,java.lang.String.class);
			//log.info("logbook "+logname);
			return logname;
	    }catch(EmptyResultDataAccessException erDAE){
	    	log.error("No such logbook defined in the db.");
	    	throw new RuntimeException("ERROR: there is no such a logbook defined in the database.");
	    }catch(DataAccessException eDAE){
	    	throw eDAE;
	    }
	}

}
