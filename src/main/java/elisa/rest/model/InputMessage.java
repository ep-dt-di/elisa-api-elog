package elisa.rest.model;

import java.util.LinkedList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * This class represent the input information provided by the user 
 * to create a new message
 * <p>
 * The class is annotated with JSR 303 annotations for data validation.
 * 
 * @author lucamag
 *
 */

@XmlAccessorType(XmlAccessType.FIELD) //Marshall only class fields into XML elements, ignore getters
@XmlRootElement(name = "input_message")
//@XmlType //Needed for the ContentViewResolver to detect object to be unmarshalled

//@XmlType(propOrder = { "field2", "field1",.. })
@XmlType( name = "input_message", 
    propOrder = {
        "subject",
        "author",
        "body",
        "status",
        "messageType",
        "systemsAffected",
        "options" })

public class InputMessage {
	
	//this class should contain the displayed properties
	//should be configurable from the form menu
	private static final Log log = LogFactory.getLog(InputMessage.class);
	   
    //Text of the message from metadata
    private String body;
    // Non optional attributes
    // - from attribute pivoting
    
    private String author;
    
    @NotNull
    @XmlElement(name="message_type")
    private String messageType;
    
    @NotNull
    private String subject;
   
    @Pattern( regexp = "open|closed", message ="Invalid status specified")
    private String status;
    
    //Sytem affected array. Made by multiple entries in attribute MID, SA_ATTR_NUMBER, VALUE1, etc.
    @NotNull 
    @XmlElement(name = "systems_affected")
    private SystemAffectedList systemsAffected = new SystemAffectedList();
    
    //List of the all optional attributes (name=value, curly bracket separated)
    private OptionList options;
    //List<Option> options = new LinkedList<Option>();
    
    
    
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
	
	//@XmlElement(name="message_type")
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	//@XmlElement(name = "systems_affected")
	public List<String> getSystemsAffected() {
		return systemsAffected==null?null:systemsAffected.getList();
	}

	public void setSystemsAffected(SystemAffectedList systemAffected) {
		this.systemsAffected = systemAffected;
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public void setStatus(String status) {
	    this.status = status;
	}
	
	public String getStatus() {
	    return status;
	}
	
	//@XmlElement(name="options")
	public OptionList getOptions() {
		return options;
	}

	public void setOptions(OptionList optattrlist) {
	    if(this.options==null)
	        this.options = new OptionList();
		this.options = optattrlist;
	}

	@Override
	public String toString() {
	    StringBuilder sb = new StringBuilder();
	    sb.append("subject: "+subject+" -- ");
	    sb.append("author: "+author+" -- ");
	    sb.append("body: "+body+" -- ");
	    sb.append("message_type: "+messageType+" -- ");
	    sb.append("systems_affected: "+systemsAffected.getListString()+" -- ");
	    
	    if(options!=null&&!options.getList().isEmpty()){
	        sb.append("options: ");
	        for(Option o:options.getList()) {
	            sb.append("[ ");
	            sb.append(o);
	            sb.append( "] ");
	        }
	    }
	   
	    return sb.toString();
	    
	}
	
}
