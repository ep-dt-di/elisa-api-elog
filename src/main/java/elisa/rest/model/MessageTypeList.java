package elisa.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="message_types")
@XmlType

public class MessageTypeList {
	
	private int count;
	private List<String> mts;

	public MessageTypeList() {}
	
	public MessageTypeList(List<String> l) {
		this.mts = l;
		this.count = l.size();
	}

	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@XmlElement(name="message_type")
	public List<String> getList() {
		return mts;
	}
	public void setList(List<String> l) {
		this.mts = l;
	}
	
	
}