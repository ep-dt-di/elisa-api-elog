package elisa.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.PROPERTY) 
@XmlRootElement(name="option_metadata")
@XmlType( name = "option_metadata", 
propOrder = {
  "name",      
  "type",
  "possibleValues", 
  "comment",
  "childOptions"})

public class OptionMetadata {
    
    static enum OptionType {
        
        SINGLEVALUE("single_value", "Choose one among the possible values"), 
        MULTIPLEVALUE("multiple_value", "Choose one or more among the possible values , in a comma separated string"), 
        FREETEXT("text", "Free text as input");
        
        private final String type;
        private final String comment;
        
        OptionType(String type, String comment) {
            //drop-down is another case of single value
            if(type.equals("drop-down"))
                this.type = "radio";
            else 
                this.type = type;
            
            this.comment=comment;
        }
        
        /**
         * This method is to be independent from database representation
         * of the option type, not possible with valueOf()
         * @param typename
         * @return
         */
        public static OptionType convert(String typename_from_db) {
            if(typename_from_db.equals("drop-down") || (typename_from_db.equals("radio")))
                return OptionType.SINGLEVALUE;
            else if(typename_from_db.equals("multiple"))
                return OptionType.MULTIPLEVALUE;
            else if(typename_from_db.equals("textbox"))
                return OptionType.FREETEXT;
            else
                return OptionType.FREETEXT;
            
            //Fix TOMCAT dep with Java7...
            //switch (typename_from_db) {
            //case "drop-down":
            //    return OptionType.SINGLEVALUE;
            //case "radio":
            //    return OptionType.SINGLEVALUE;
            ///case "multiple":
            //    return OptionType.MULTIPLEVALUE;
            //case "textbox":
            //    return OptionType.FREETEXT;
            //default:
            //    return OptionType.FREETEXT;
            //}
        }
    }
    
    private String name;
    private String value;
    private OptionType type;
    private Long ID;
    
    private OptionMetadataList innerOptions; //Lazy initialization
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    @XmlElement(name="possible_values")
    public String getPossibleValues() {
        return value;
    }
    public void setPossibleValues(String value) {
        this.value = value;
    }
    
    public String getType() {
        return this.type.name();
    }
    
    public void setType(String type) {
        this.type = OptionType.convert(type);
    }
    
    @XmlElement(name="comment")
    public String getComment() {
        return this.type.comment;
    }
    
    @XmlElement(name="options")
    public OptionMetadataList getChildOptions() {
        return this.innerOptions;
    }
    
    public void setChildOptions(List<OptionMetadata> opts) {
        if(this.innerOptions==null)
                this.innerOptions = new OptionMetadataList();
        this.innerOptions.setList(opts);
    }
  
    public void setID(long long1) {
        this.ID=long1;
    }
   
    @XmlTransient
    public Long getID() {
        return this.ID;
    }
    

}
