package elisa.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.StringUtils;

@XmlRootElement(name="systems_affected")
@XmlType

public class SystemAffectedList {
	
	private int count;
	private List<String> sa;

	public SystemAffectedList() {}
	
	public SystemAffectedList(List<String> l) {
		this.sa = l;
		this.count = l.size();
	}

	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@XmlElement(name="system_affected")
	public List<String> getList() {
		return sa;
	}
	public void setList(List<String> l) {
		this.sa = l;
		this.count = sa.size();
	}
	
	public String getListString() {
	    return StringUtils.join(this.sa,  " | ");
	}
	
	
}