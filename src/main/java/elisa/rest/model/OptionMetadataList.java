package elisa.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="options_metadata")
@XmlType

public class OptionMetadataList {
	
	private int count;
	private List<OptionMetadata> opts;

	public OptionMetadataList() {}
	
	public OptionMetadataList(List<OptionMetadata> l) {
		this.opts = l;
		this.count = l.size();
	}

	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@XmlElement(name="option")
	public List<OptionMetadata> getList() {
		return opts;
	}
	public void setList(List<OptionMetadata> l) {
		this.opts = l;
		this.count = l.size();
	}
	
	
}