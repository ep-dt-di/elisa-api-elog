package elisa.rest.model;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="message_body")
@XmlType

public class MessageBody {
    
    @NotNull
    private String body;
    
    public MessageBody() {}
    
    @XmlElement(name="body")
    public String getText() {
        return this.body;
    }
    
    public void setText(String t) {
        this.body = t;
    }

}
