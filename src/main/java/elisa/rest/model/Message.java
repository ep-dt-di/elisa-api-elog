package elisa.rest.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import elisa.service.Utils;


@XmlAccessorType(XmlAccessType.FIELD) //Marshall only class fields into XML elements, ignore getters
@XmlRootElement(name = "message")
//@XmlType //Needed for the ContentViewResolver to detect object to be unmarshalled

//@XmlType(propOrder = { "field2", "field1",.. })
@XmlType( name = "message", 
    propOrder = {
        "author",
        "username",
        "subject",
        "date",
        "body",
        "messageType", 
        "systemAffected", 
        "id", 
        "logbook", 
        "encoding", 
        "hasAttachments",
        "hasReplies", 
        "replyTo",
        "threadHead", 
        "valid", 
        "status",
        "host_ip",
        "attachments",
        "optionalAttributes"
    })

/**
 * 
 * <p/>
 * Every field is mapped into a XML element with the same name.
 * XMLElement annotation is used to ovveride element name.
 *  
 * @author lucamag
 *
 */
@Component
public class Message {
	
	//this class should contain the displayed properties
	//should be configurable from the form menu
	private static final Log log = LogFactory.getLog(Message.class.getName());
	
	//Id of the message
    private long id;
    
    private static Utils utils = new Utils();
	
    //Thread ID of the top thread ID
    @XmlElement(name="thread_head")
    private long threadHead;
    
    //Logbook
    private  int logbook;
    
    //Kind of boolean, from metadata
    @XmlElement(name="has_replies")
    private  int hasReplies;
    
    //Message ID of the parent in the thread
    @XmlElement(name="reply_to")
    private  long replyTo;
    
    //Kind of boolean 
    @XmlElement(name="has_attachments")
    private  int hasAttachments;
    //Encoding
    private  int encoding;
    //Valid
    private  String valid;
  
    //Date coming from metadata
    
    @XmlElement(name="date")
    private Date date;
    
    //Text of the message from metadata
    private String body;
    
    // Non optional attributes
    @XmlElement(name="message_type")
    private String messageType;
    
    private String subject;
    private String author;
    //@XmlTransient
    private String status;
    @XmlElement(name="username")
    private String username;
    @XmlElement(name="host_ip")
    private String host_ip;

    //Sytem affected array. Made by multiple entries in attribute MID, SA_ATTR_NUMBER, VALUE1, etc.
    @XmlElement(name = "systems_affected")
    private SystemAffectedList systemAffected = new SystemAffectedList(); 
    //private List<String> systemAffected;
    
    //List of the all optional attributes (name=value, curly bracket separated)
    @XmlElement(name="options")
    private OptionList optionalAttributes;
    
    @XmlElement(name="attachments")
    private AttachmentList attachments ;
    
    
    public Message() {
        //logbook = 70;
        hasReplies = 0;
        replyTo = 0;
        hasAttachments = 0;
        encoding = 3;
        valid="valid";
        status="open";
        
    }
    
	
	public long getID() {
		return id;
	}

	public void setID(long id) {
		this.id = id;
	}
	
	public int getLogbook() {
        return logbook;
    }

    public void setLogbook(int i) {
        this.logbook=i;
    }

    public Date getDate() {
        return this.date;
    }
    
    public void setDate(Timestamp timestamp) {
        long milliseconds = timestamp.getTime() + (timestamp.getNanos() / 1000000);
        this.date = new Date(milliseconds);
    }


	public int getHasReplies() {
		return hasReplies;
	}

	public void setHasReplies(int hasReplies) {
		this.hasReplies = hasReplies;
	}

	public long getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(long replyTo) {
		this.replyTo = replyTo;
	}

	public long getThreadHead() {
		return threadHead;
	}

	public void setThreadHead(long threadHead) {
		this.threadHead = threadHead;
	}

	public int getHasAttachments() {
		return hasAttachments;
	}

	public void setHasAttachments(int hasAttachments) {
		this.hasAttachments = hasAttachments;
	}

	public int getEncoding() {
	    return encoding;
	}
	
	public void setEncoding(int e) {
	    this.encoding=e;
	}

	public String getValid() {
        return valid;
    }
    public void setValid(String v) {
        this.valid=v;
    }

	public String getBody() {
		return  body;
	}
	public void setBody(String body) {
		this.body = body;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getHost_ip() {
		return host_ip;
	}

	public void setHost_ip(String host_ip) {
		this.host_ip = host_ip;
	}

	public SystemAffectedList getSystemAffected() {
		return systemAffected;
	}

	public void setSystemAffected(List<String> saArr) {
		this.systemAffected.setList(saArr);
	}
	
	public void setSystemAffectedFromString(String sa_string) {
	    //From database, the system affected string is a "|" separated list of system affected
	    List<String> sa = new LinkedList<String>();
	    //if(sa_string==null)
	        //log.error("WTF?????????");
	    String[] row_sa = sa_string==null?null:sa_string.replaceAll("\\[", "").replaceAll("\\]","").split("\\;");
	    if(row_sa==null)
	        sa.add(sa_string);
	    else 
	        for(String s:row_sa)
	            sa.add(s);
	    
	    this.systemAffected.setList(sa);
	}
	
	public String getSystemAffectedAsString() {
	    return StringUtils.join(this.systemAffected.getList(), ',');
    }
	

	public OptionList getOptionalAttributes() {
		return optionalAttributes;
	}

	public void setOptionalAttributes(List<Option> optattrlist) {
	    if(this.optionalAttributes==null)
	        this.optionalAttributes = new OptionList();
		this.optionalAttributes.setList(optattrlist);
	}

	
	public AttachmentList getAttachments() {
	    return this.attachments;
	}
	
	public void setAttachmentList(AttachmentList list) {
	    if (attachments==null)
	            this.attachments= new AttachmentList();
	    this.attachments = list;
	}
   
	public void addAttachment(Attachment a) {
	    this.attachments.getList().add(a);
	}
	
	public String getBodyHTML() {
	    return utils.markdown2html(body);
	}

	
}
