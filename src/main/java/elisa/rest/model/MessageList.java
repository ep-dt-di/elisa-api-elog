package elisa.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="messages")
@XmlType
public class MessageList {
	
	private int count;
	private List<Message> messages;
	private String prev;
	private String next;

	public MessageList() {}
	
	public MessageList(String prev, String next, List<Message> messages) {
		this.prev = prev;
		this.next = next;
		this.messages = messages;
		this.count = messages.size();
	}

	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@XmlElement(name="message")
	public List<Message> getMessages() {
		return messages;
	}
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	public String getPrev() {
		return prev;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}
	
}