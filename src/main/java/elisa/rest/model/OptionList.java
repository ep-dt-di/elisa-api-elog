package elisa.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="options")
@XmlType

public class OptionList {
	
	private int count;
	private List<Option> opts;

	public OptionList() {}
	
	public OptionList(List<Option> opts) {
		this.opts = opts;
		this.count = opts.size();
	}

	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@XmlElement(name="option")
	public List<Option> getList() {
		return opts;
	}
	public void setList(List<Option> opts) {
		this.opts = opts;
		this.count = opts.size();
	}
	
	@Override
	public String toString() {
	    String s = "Options: \n";
	    for(Option opt:opts)
	        s = s+" ["+opt+"]";
        return s;
	    
	}
	
	
}