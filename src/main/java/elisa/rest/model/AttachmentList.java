package elisa.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name="attachments")
@XmlType

public class AttachmentList {
	
	private int count;
	private List<Attachment> attachments;

	public AttachmentList() {}
	
	public AttachmentList(List<Attachment> l) {
		this.attachments = l;
		this.count = l.size();
	}

	@XmlElement(name="count")
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@XmlElement(name="attachment")
	public List<Attachment> getList() {
		return attachments;
	}
	public void setList(List<Attachment> a) {
		this.attachments = a;
	}
	
	
}