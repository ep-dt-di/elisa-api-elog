package elisa.rest.model;

/**
 * 
 * Message Type 
 * 
 * <p>
 * To be extended with 
 * http://pauloswald.com/blog/documents/26/java-5-database-backed-enumerations
 * @author lucamag
 *
 */

public enum MessageType {
    ONLINE("Online", 147L),
    SHIFT_SUMMARY("Shift Summary",111L);
    
    private final String name;
    private final long id;
    
    //Constructor
    MessageType(String name, Long id) {
        this.id=id;
        this.name=name;
        //Do something dynamic? Retrieve ID from DB?
    }
    
}
