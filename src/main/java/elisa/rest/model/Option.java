package elisa.rest.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.PROPERTY) 
@XmlRootElement(name="option")
@XmlType( name = "option", 
propOrder = {
  "name",      
  "value",
  "options"})

public class Option {
    
    private String name;
    private String value;
    @NotNull
    private OptionList options; //Lazy initialization
    private OptionMetadata metadata;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
       
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    
    @XmlElement(name="options")
    public OptionList getOptions() {
        return this.options;
    }
    
    public void setOptions(OptionList opts) {
        if(this.options==null)
                this.options = new OptionList();
        this.options = opts;
    }
    
    @XmlTransient
    public OptionMetadata getOptionMetadata() {
        return this.metadata;
    }
    
    public void setOptionMetadata(OptionMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "name: "+name+", value:" +value+(options==null?"":(", options: "+options));
    }

    
 }
