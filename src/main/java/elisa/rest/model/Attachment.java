package elisa.rest.model;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name="attachment")
@XmlType

public class Attachment {
    
    private  Long ID; 
    private  String fileName;
    private  byte[] barray;
    private  String link; 
    
    public Attachment() {};
    public Attachment(Long ID, String fileName,  byte[] ba) {
        this.ID=ID;
        this.fileName = fileName;
        this.barray = ba;
    }
    
    @XmlElement(name="filename")
    public String getFileName(){
        return this.fileName;
    }
    
    @XmlTransient
    public byte[] getByteArray(){
        return this.barray;
    }
    
    public void setByteArray(byte[] ba) {
        this.barray =ba;
    }
    
    @XmlElement(name="link")
    public String getLink() {
        return this.link;
    }
    
    public void setLink(String link) {
        this.link  = link;
    }

    public void setID(Long id) {
        this.ID=id;
    }
    
    public Long getID() {
        return this.ID;
    }
    
    public void setFileName(String filename) {
        this.fileName = filename;
    }

}
