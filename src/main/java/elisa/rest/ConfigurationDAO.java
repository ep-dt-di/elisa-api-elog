package elisa.rest;

import java.util.List;

import org.springframework.stereotype.Repository;

import elisa.rest.model.OptionMetadata;

@Repository //This assure the Exception translation from Spring
public interface ConfigurationDAO {
    
    public List<String> getMessageTypes();
    public List<String> getMessageTypes(int logbookID);
    
    public List<String> getSystemAffected(String messageType);
    
    public List<OptionMetadata> getOptionsMetadata(String messageType);

    public List<OptionMetadata> getSecondLevelOptionsMetadata(String messageType,
            String optionName);

    public OptionMetadata getOptionMetadata(String messageType, String optionName);

    public List<OptionMetadata> getSecondLevelOptionMetadata(String messageType, String optionName,
            String optionValue);

    public List<String> getSystemAffected(int logbookID);
    public List<String> getSystemAffected();
    

	String getEmailrecipientsBySAName(String sa_name);	
	String getEmailrecipientsByMTName(String mt_type);
	String getPresetEmailrecipients(String option_name, String option_value, String option_parent);
    
	int getLogbookID(String logbookname);
	String getLogbookName(int logbookid);

}
