package elisa.rest;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.transaction.annotation.Transactional;

import elisa.rest.model.Attachment;
import elisa.rest.model.AttachmentList;
import elisa.rest.model.Message;
import elisa.rest.model.Option;
import elisa.rest.model.OptionList;
import elisa.rest.model.OptionMetadata;


public class JdbcMessageDAO implements MessageDAO {

    private static final Log log = LogFactory.getLog(JdbcMessageDAO.class.getName());
    
    @Autowired
    private ConfigurationDAO configurationDAO; //used for optional attributes retrieval
    
    private final DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    private static final int DEFAULT_NUMBER_MESSAGES = 100;
    
    private static final String DEFAULT_LOGBOOK_NAME="ATLAS";
    private static final int DEFAULT_LOGBOOK_ID=70;
    
    private static final String DATE_FORMAT_Trim = "yyMMdd_HHmmss_";
    
    public JdbcMessageDAO(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }


    /**
     * Return the message information for the specified ID.
     * 
     * @param Long ID 
     * @return the message
     */
    
    @Cacheable("messages")
    @Override
    public Message getMessage(int logbookID, Long ID) throws DataAccessException {
        log.debug("Retrieving from logbook "+logbookID+" the message ID= "+ID);
        
        String sql = "SELECT MESSAGE.MESS_ID, MESSAGE.DATE_TIME, ATTRIBUTES.AUTHOR, ATTRIBUTES.SUBJECT, " +
                "ATTRIBUTES.SYSTEM_AFFECTED, ATTRIBUTES.MESSAGE_TYPE,MESSAGE.THREAD_HEAD, MESSAGE.HAS_REPLIES, "+
                "MESSAGE.REPLY_TO, MESSAGE.HAS_ATTACHMENTS, ATTRIBUTES.STATUS, ATTRIBUTES.USERN, MESSAGE.BODY "+
                "FROM "+ 
                "(SELECT ATL2_METADATA.MESS_ID," +
                "ATL2_METADATA.DATE_TIME," +
                "ATL2_METADATA.THREAD_HEAD,"+ 
                "ATL2_METADATA.HAS_REPLIES,"+ 
                "ATL2_METADATA.REPLY_TO,"+ 
                "ATL2_METADATA.HAS_ATTACHMENTS,"+ 
                "ATL2_METADATA.BODY "+
                "FROM   ATL2_METADATA ATL2_METADATA "+
                "WHERE  ATL2_METADATA.LOGBOOK = :logbook "+ 
                " AND ATL2_METADATA.MESS_ID = :message_id "+
                "ORDER by ATL2_METADATA.MESS_ID DESC) MESSAGE "+
                "INNER JOIN "+
                "(SELECT M.MESS_ID,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 7, ATL2_ATTRIBUTES.ATTR_VALUE, NULL ) ) AS AUTHOR,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 5, ATL2_ATTRIBUTES.ATTR_VALUE, NULL ) ) AS USERN,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 23, ATL2_ATTRIBUTES.ATTR_VALUE, NULL ) ) AS SUBJECT,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 12, ATL2_STRING2ID_ATTR_OPT.STR, NULL ) ) AS STATUS,"+
                "LISTAGG(decode(ATL2_ATTRIBUTES.ATTR_NAME, 14, ATL2_STRING2ID_ATTR_OPT.STR, NULL ) , \';\')"+ 
                "WITHIN GROUP (ORDER BY M.MESS_ID) AS SYSTEM_AFFECTED,"+ 
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 772, ATL2_STRING2ID_ATTR_OPT.STR, NULL ) ) AS MESSAGE_TYPE "+
                " FROM "+
                "(SELECT  ATL2_METADATA.MESS_ID, ATL2_METADATA.LOGBOOK "+
                " FROM ATL2_METADATA ATL2_METADATA "+
                " WHERE ATL2_METADATA.LOGBOOK = :logbook "+
                " AND ATL2_METADATA.MESS_ID = :message_id "+
                "ORDER by ATL2_METADATA.MESS_ID DESC) m,"+ 
                "ATL2_ATTRIBUTES, "+
                "ATL2_STRING2ID_ATTR_OPT "+
                "WHERE ATL2_ATTRIBUTES.LOGBOOK = M.LOGBOOK "+
                "AND M.MESS_ID=ATL2_ATTRIBUTES.MESS_ID "+ 
                "AND ATL2_ATTRIBUTES.ATTR_OPT = ATL2_STRING2ID_ATTR_OPT.ID "+ 
                "GROUP BY M.MESS_ID "+
                "ORDER by M.MESS_ID DESC) ATTRIBUTES "+
                "ON MESSAGE.MESS_ID = ATTRIBUTES.MESS_ID";  

        Message m = namedJdbcTemplate.queryForObject(sql,
                new MapSqlParameterSource().addValue("message_id", ID).addValue("logbook", logbookID),
                new MessageMapper());
        m.setLogbook(logbookID);
        
        
        /******* TODO
         * Do this also when getting message list
         * Or we may decide to do that with an option like ?show_metadata=true...
         *********/
        
        //Get Attachments information
        List<Attachment> attachments = getAttachmentsMetadata(logbookID, ID);
        if(!attachments.isEmpty())
            m.setAttachmentList(new AttachmentList(attachments));
        
        //Get optional attributes
        List<Option> options =  getOptions(m);
        if(options!=null&&!options.isEmpty()) {
            m.setOptionalAttributes(options);
        }
        //log.debug("Message with ID "+ID +"from logbook "+m.getLogbook()+" successfully retrieved from DB with "+(options==null?"0":options.size())+" optional paramters.");
        return  m;


    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Message createMessage(Message m) {
        log.trace("Creating new message with subject " + m.getSubject());

        //log.debug("logbookid="+m.getLogbook());
        //Get the next message ID, and atomically increment it
        //TODO be atomic!
        long nextID = jdbcTemplate.queryForInt("SELECT NEXT_ID FROM ATL2_MID_SEQUENCE WHERE LOGBOOK= "+m.getLogbook()+" ");
        try {
        	int count = jdbcTemplate.update("UPDATE ATL2_MID_SEQUENCE SET NEXT_ID=NEXT_ID + 1 WHERE LOGBOOK= "+m.getLogbook()+" ");
        	if(count==0){
            	log.error("Can not set the next message id.");
            	throw new RuntimeException("ERROR: can not set the next message id.");        		
        	}
        }catch(DataAccessException eDAE){
            log.error(eDAE.getMessage());
        	throw new RuntimeException(eDAE.getMessage());        		
        }   
        
        log.trace("Next message ID :"+nextID);

        
        if(m.getThreadHead()==0L)
            m.setThreadHead(nextID);

        String insert_metadata =
                "INSERT INTO "+
                        "ATL2_METADATA ( LOGBOOK, MESS_ID, DATE_TIME, HAS_REPLIES, REPLY_TO, THREAD_HEAD, HAS_ATTACHMENTS, ENCODING, BODY )"+
                        "VALUES (:logbook,"+nextID+",:date,:hasReplies, :replyTo, :threadHead, :hasAttachments, :encoding, :body)";

        
        //This extract the paramter from the Message bean using
        //standard getter.  Be careful on JavaBean name convention
        //getLogbook() -> logbook        
        //SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(m); 
        
        try {
            //int count = namedJdbcTemplate.update(insert_metadata, namedParameters);
        	int count = namedJdbcTemplate.update(insert_metadata, new MapSqlParameterSource()
                    .addValue("logbook",m.getLogbook())
                    .addValue("date",m.getDate())
                    .addValue("hasReplies",m.getHasReplies())
                    .addValue("replyTo",m.getReplyTo())
                    .addValue("threadHead",m.getThreadHead())
                    .addValue("hasAttachments",m.getHasAttachments())
                    .addValue("encoding",m.getEncoding())
                    .addValue("body",m.getBody()));
        	if(count==0){
            	log.error("Can not update metadata table.");
            	throw new RuntimeException("ERROR: can not update metadata table.");        		
        	}
        //this method does not throw EmptyResultDataAccessException, only DataAccessException
        //}catch(EmptyResultDataAccessException erDAE){
        //    log.error(erDAE.getMessage());   //throw erDAE;
        }catch(DataAccessException eDAE){
            log.info(eDAE.getMessage());
        	throw new RuntimeException(eDAE.getMessage());        		
        }   

        log.trace("Author: "+StringEscapeUtils.escapeSql(m.getAuthor()));

        String insert_mandat_attributes =
                "INSERT FIRST "+
                        "WHEN attr_id=7 THEN "+
                        "INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                        "VALUES(:LOGBOOK, :MESS_ID, attr_id, 0, '"+StringEscapeUtils.escapeSql(m.getAuthor())+"') "+
                        
                        "WHEN attr_id=5 THEN "    +
                        "INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                        "VALUES(:LOGBOOK, :MESS_ID, attr_id, 0, '"+m.getUsername()+"') "+

                        "WHEN attr_id=6 THEN "    +
                        "INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                        "VALUES(:LOGBOOK, :MESS_ID, attr_id, 0, '"+m.getHost_ip()+"') "+

                        "WHEN attr_id=23 THEN "   +
                        "INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                        "VALUES(:LOGBOOK, :MESS_ID, attr_id, 0, '"+StringEscapeUtils.escapeSql(m.getSubject())+"')" +
                        
                        "SELECT ATL2_STRING2ID_ATTR_NAME.ID  attr_id "+
                        "FROM ATL2_STRING2ID_ATTR_NAME ATL2_STRING2ID_ATTR_NAME "+
                        "WHERE ATL2_STRING2ID_ATTR_NAME.STR = 'USER' "+
                        "OR ATL2_STRING2ID_ATTR_NAME.STR = 'REM_IP' "+
                        "OR ATL2_STRING2ID_ATTR_NAME.STR = 'SUBJECT' " +
                        "OR ATL2_STRING2ID_ATTR_NAME.STR = 'AUTHOR'";

        try {
            int count = namedJdbcTemplate.update(insert_mandat_attributes, new MapSqlParameterSource()
            .addValue("LOGBOOK",m.getLogbook())
            .addValue("MESS_ID",nextID));
        	if(count!=4){
            	log.error("Can not insert Author or Username or hostip or Subject.");
            	throw new RuntimeException("ERROR: can not insert Author or Username or hostip or Subject.");        		
        	}
       //this method does not throw EmptyResultDataAccessException, only DataAccessException
        //}catch(EmptyResultDataAccessException erDAE){
        //    log.error(erDAE.getMessage());//throw erDAE;
        }catch(DataAccessException eDAE){
            log.error(eDAE.getMessage());       
        	throw new RuntimeException(eDAE.getMessage());        		
        }     

        String insert_mt =
                "INSERT INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                        "SELECT :LOGBOOK, :MESS_ID, ATL2_STRING2ID_ATTR_NAME.ID, ATL2_STRING2ID_ATTR_OPT.ID, NULL "+
                        "FROM ATL2_STRING2ID_ATTR_NAME, ATL2_STRING2ID_ATTR_OPT "+
                        "WHERE (ATL2_STRING2ID_ATTR_NAME.STR='MESSAGE TYPE' AND ATL2_STRING2ID_ATTR_OPT.STR= '"+m.getMessageType()+"') "+ 
                        "OR (ATL2_STRING2ID_ATTR_NAME.STR = 'STATUS' AND ATL2_STRING2ID_ATTR_OPT.STR= '"+m.getStatus()+"') "+
                        "OR (ATL2_STRING2ID_ATTR_NAME.STR = 'VALID' AND ATL2_STRING2ID_ATTR_OPT.STR= '"+m.getValid()+"') "+
                        "OR (ATL2_STRING2ID_ATTR_NAME.STR = 'AGENT_TYPE' AND ATL2_STRING2ID_ATTR_OPT.STR= 'api') ";

        try {
            int count = namedJdbcTemplate.update(insert_mt, new MapSqlParameterSource()
            .addValue("LOGBOOK",m.getLogbook())
            .addValue("MESS_ID",nextID));
            //validation for Status is done at InputMessage.
        	if(count!=4){
            	log.error("Can not insert Message Type  or Status or Validity or Agent_type.");
            	throw new RuntimeException("ERROR: can not insert Message Type  or Status or Validity or Agent_type.");        		
        	}
           
        //this method does not throw EmptyResultDataAccessException, only DataAccessException
        //}catch(EmptyResultDataAccessException erDAE){
        //    log.error(erDAE.getMessage());  //throw erDAE;
        }catch(DataAccessException eDAE){
           log.error(eDAE.getMessage());  
           throw new RuntimeException(eDAE.getMessage());
        } 


//        /***********************************
//         * Conditional insert of the System affected type if not existing, just for the test database, to be removed in production
//         */
//
//        //Check if the System Affected string exists
//        long id = namedJdbcTemplate.queryForLong("SELECT COUNT(*) FROM ATL2_STRING2ID_ATTR_OPT WHERE ATL2_STRING2ID_ATTR_OPT.STR=:ATTR_OPT_STR",
//                new MapSqlParameterSource().addValue("ATTR_OPT_STR",m.getSystemAffectedAsString()));
//
//        if (id<=0) {
//            //Get last ID
//            long lastID = namedJdbcTemplate.queryForLong("SELECT * FROM ( SELECT ID FROM ATL2_STRING2ID_ATTR_OPT ORDER BY ID DESC) WHERE ROWNUM<=1",
//                    new MapSqlParameterSource().addValue("ATTR_OPT_STR",m.getSystemAffectedAsString()));
//            String insert_sa_type =
//                    "INSERT "+
//                            "INTO ATL2_STRING2ID_ATTR_OPT ( STR, ID) "+
//                            "VALUES(:ATTR_OPT_STR, :ID)";
//            int count = namedJdbcTemplate.update(insert_sa_type, new MapSqlParameterSource()
//            .addValue("ATTR_OPT_STR",m.getSystemAffectedAsString())
//            .addValue("ID",lastID+1));
//
//        }
//        /*********************************/

        for(String sa:m.getSystemAffected().getList()) {
            log.info("System affected to insert: "+sa);
        	String insert_sa =
                "INSERT INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                        "SELECT :LOGBOOK, :MESS_ID, 14, ATL2_STRING2ID_ATTR_OPT.ID, NULL "+
                        "FROM ATL2_STRING2ID_ATTR_OPT "+
                        "WHERE ATL2_STRING2ID_ATTR_OPT.STR=:ATTR_OPT_STR ";
        try {
            int count = namedJdbcTemplate.update(insert_sa, new MapSqlParameterSource()
            .addValue("LOGBOOK",m.getLogbook())
            .addValue("MESS_ID",nextID)
            .addValue("ATTR_OPT_STR",sa));
            if (count==0 ) {
            	log.error("no SA inserted, invalid value: "+sa);
            	throw new RuntimeException("ERROR: System_Affected invalid: "+sa);
            }
        //this method does not throw EmptyResultDataAccessException, only DataAccessException
        //}catch(EmptyResultDataAccessException erDAE){
        //    log.error(erDAE.getMessage());  //throw erDAE;
        }catch(DataAccessException eDAE){
            log.error(eDAE.getMessage());  
            throw new RuntimeException(eDAE.getMessage());
        } 
           
        }
         
        /****** Insert Optional attributes **********/
        log.trace("Insertion additional options");
        if(m.getOptionalAttributes()!=null)
            for(Option o: m.getOptionalAttributes().getList()) {
                
                log.trace("Insertion additional options");

                String sql ="";
                MapSqlParameterSource map = new MapSqlParameterSource();

                // If option is a free text
                if(o.getOptionMetadata().getType().equals("FREETEXT")) {

                    //Get option ID
                    sql = "INSERT INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                            "SELECT :LOGBOOK, :MESS_ID, ATL2_STRING2ID_ATTR_NAME.ID , 0 , :TEXT "+
                            "FROM ATL2_STRING2ID_ATTR_NAME "+
                            "WHERE ATL2_STRING2ID_ATTR_NAME.STR=:OPT_NAME ";
                    map.addValue("LOGBOOK", m.getLogbook()).addValue("MESS_ID", nextID).addValue("OPT_NAME", o.getName().toUpperCase()).addValue("TEXT", o.getValue());

                } else {
                    //or option is a predefined value
                    sql = "INSERT INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                            "SELECT :LOGBOOK, :MESS_ID, ATL2_STRING2ID_ATTR_NAME.ID , ATL2_STRING2ID_ATTR_OPT.ID  , NULL "+
                            "FROM ATL2_STRING2ID_ATTR_NAME,  ATL2_STRING2ID_ATTR_OPT "+
                            "WHERE ATL2_STRING2ID_ATTR_NAME.STR=:OPT_NAME AND ATL2_STRING2ID_ATTR_OPT.STR = :OPT_VALUE";
                    
                    log.trace("Data for option insertion: Message ID: "+nextID+", OPT_NAME: "+o.getName().toUpperCase()+", OPT_VALUE: "+o.getValue());
                    map.addValue("LOGBOOK", m.getLogbook()).addValue("MESS_ID", nextID).addValue("OPT_NAME", o.getName().toUpperCase()).addValue("OPT_VALUE", o.getValue());

                }    

                try {
                    int count = namedJdbcTemplate.update(sql, map);
                    if (count==0 ) {
                    	log.error("no Option inserted, invalid value: "+o.getValue());
                    	throw new RuntimeException("ERROR: Option invalid: "+o.getValue());
                    }
                //this method does not throw EmptyResultDataAccessException, only DataAccessException
                //}catch(EmptyResultDataAccessException erDAE){
                //    log.error(erDAE.getMessage());//throw erDAE;
                }catch(DataAccessException eDAE){
                    log.error(eDAE.getMessage());         
                    throw new RuntimeException(eDAE.getMessage());
               } 
                
                //Insert inner options
                
                if(o.getOptions()!=null)
                    for(Option inneropt: o.getOptions().getList()) {
                        
                        log.trace("Insertion additional inner options for "+o.getName());

                        String innersql ="";
                        MapSqlParameterSource innermap = new MapSqlParameterSource();

                        // If option is a free text
                        if(inneropt.getOptionMetadata().getType().equals("FREETEXT")) {

                            //Get option ID
                            innersql = "INSERT INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                                    "SELECT :LOGBOOK, :MESS_ID, ATL2_STRING2ID_ATTR_NAME.ID , 0 , :TEXT "+
                                    "FROM ATL2_STRING2ID_ATTR_NAME "+
                                    "WHERE ATL2_STRING2ID_ATTR_NAME.STR=:OPT_NAME ";
                            innermap.addValue("LOGBOOK", m.getLogbook()).addValue("MESS_ID", nextID).addValue("OPT_NAME", inneropt.getName().toUpperCase()).addValue("TEXT", inneropt.getValue());

                        } else {
                            //or option is a predefined value
                            innersql = "INSERT INTO ATL2_ATTRIBUTES ( LOGBOOK, MESS_ID, ATTR_NAME, ATTR_OPT, ATTR_VALUE )"+
                                    "SELECT :LOGBOOK, :MESS_ID, ATL2_STRING2ID_ATTR_NAME.ID , ATL2_STRING2ID_ATTR_OPT.ID  , NULL "+
                                    "FROM ATL2_STRING2ID_ATTR_NAME,  ATL2_STRING2ID_ATTR_OPT "+
                                    "WHERE ATL2_STRING2ID_ATTR_NAME.STR=:OPT_NAME AND ATL2_STRING2ID_ATTR_OPT.STR = :OPT_VALUE";
                            
                            log.trace("Data for option insertion: Message ID: "+nextID+", OPT_NAME: "+inneropt.getName().toUpperCase()+", OPT_VALUE: "+inneropt.getValue());
                            innermap.addValue("LOGBOOK", m.getLogbook()).addValue("MESS_ID", nextID).addValue("OPT_NAME",inneropt.getName().toUpperCase()).addValue("OPT_VALUE", inneropt.getValue());

                        }    

                        try {
                            int count = namedJdbcTemplate.update(innersql, innermap);
                            if (count==0 ) {
                            	log.error("no Inner Option inserted, invalid value: "+inneropt.getValue());
                            	throw new RuntimeException("ERROR: Inner Option invalid: "+inneropt.getValue());
                            }
                        //this method does not throw EmptyResultDataAccessException, only DataAccessException
                        //}catch(EmptyResultDataAccessException erDAE){
                        //    log.error(erDAE.getMessage());//throw erDAE;
                        }catch(DataAccessException eDAE){
                            log.error(eDAE.getMessage());         
                            throw new RuntimeException(eDAE.getMessage());
                       } 
                        
                    }

            }
    


        return this.getMessage(m.getLogbook(),nextID);

    }

    @Override
    public void deleteMessage(Long ID) {
        //Next Message ID
        namedJdbcTemplate.update("delete from ATL2_ATTRIBUTES where MESS_ID=:id and logbook=70",new MapSqlParameterSource().addValue("id", ID));
        namedJdbcTemplate.update("delete from ATL2_ATTACHMENTS where MESS_ID=:id and logbook=70",new MapSqlParameterSource().addValue("id", ID));
        namedJdbcTemplate.update("delete from ATL2_METADATA where MESS_ID=:id and logbook=70",new MapSqlParameterSource().addValue("id", ID));
        
    }

    @Override
    public void updateMessage(Long ID, Message m) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Message> findByQuery(QueryParams qp) {
        if(qp.hasFilterCriteria()){
            log.info("With filtering");
        	return this.getMessagesWithFiltering(qp);
        }    
        else {
        	log.info("No filtering");
            return this.getMessagesNoFiltering(qp.getNumberOfMessagesToReturn(), qp.getPage(), qp.getNumberOfMonthsToQuery(),qp.getLogbookID());
        }    
    }



    /**
     * Get the last n messages ordered by decreasing time.
     * Please note that the query has a 1 month hard limit built-in.
     * @param numberOfMessages the number of messages to be retrieved
     * @return the list of messages
     */
    private  List<Message> getMessagesNoFiltering(Integer numberOfMessages, Integer page, Integer timeinterval, Integer logbookID){

        log.info("Retrieving messages with no filtering query...");
         
        //At this stage numberOfMessage may be null
        if(numberOfMessages==null)
            numberOfMessages=DEFAULT_NUMBER_MESSAGES;

        if(logbookID==null)
        	logbookID=DEFAULT_LOGBOOK_ID;
        	
        String sql = "SELECT MESSAGE.MESS_ID, MESSAGE.DATE_TIME, ATTRIBUTES.AUTHOR, ATTRIBUTES.SUBJECT, " +
                "ATTRIBUTES.SYSTEM_AFFECTED, ATTRIBUTES.MESSAGE_TYPE,MESSAGE.THREAD_HEAD, MESSAGE.HAS_REPLIES, "+
                "MESSAGE.REPLY_TO, MESSAGE.HAS_ATTACHMENTS, ATTRIBUTES.STATUS, ATTRIBUTES.USERN," +
                " MESSAGE.BODYCLOB "+
                "FROM "+ 
                "(SELECT * FROM "+
                "(SELECT  ATL2_METADATA.MESS_ID," +
                "ATL2_METADATA.DATE_TIME," +
                "ATL2_METADATA.THREAD_HEAD,"+ 
                "ATL2_METADATA.HAS_REPLIES,"+ 
                "ATL2_METADATA.REPLY_TO,"+ 
                "ATL2_METADATA.HAS_ATTACHMENTS,"+ 
                "ATL2_METADATA.BODY AS BODYCLOB "+
                "FROM   ATL2_METADATA ATL2_METADATA "+
                "WHERE  ATL2_METADATA.LOGBOOK = " + logbookID +
                (timeinterval!=null?" AND ATL2_METADATA.DATE_TIME > ADD_MONTHS(SYSDATE, -"+timeinterval+" ) ":" ")+
                (page!=null&&page>1?" AND ATL2_METADATA.MESS_ID <= :last_id ":" ")+
                "ORDER by ATL2_METADATA.MESS_ID DESC)"+  
                "WHERE ROWNUM <=:num_of_rows ) MESSAGE "+
                "INNER JOIN "+
                "(SELECT M.MESS_ID,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 7, ATL2_ATTRIBUTES.ATTR_VALUE, NULL ) ) AS AUTHOR,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 5, ATL2_ATTRIBUTES.ATTR_VALUE, NULL ) ) AS USERN,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 23, ATL2_ATTRIBUTES.ATTR_VALUE, NULL ) ) AS SUBJECT,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 12, ATL2_STRING2ID_ATTR_OPT.STR, NULL ) ) AS STATUS,"+
                "LISTAGG(decode(ATL2_ATTRIBUTES.ATTR_NAME, 14, ATL2_STRING2ID_ATTR_OPT.STR, NULL ) , \';\') "+ 
                "WITHIN GROUP (ORDER BY M.MESS_ID) AS SYSTEM_AFFECTED,"+ 
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 772, ATL2_STRING2ID_ATTR_OPT.STR, NULL ) ) AS MESSAGE_TYPE "+
                " FROM "+
                "(SELECT * FROM "+ 
                "(SELECT  ATL2_METADATA.MESS_ID, ATL2_METADATA.LOGBOOK "+
                " FROM ATL2_METADATA ATL2_METADATA "+
                " WHERE ATL2_METADATA.LOGBOOK = " + logbookID +
                (timeinterval!=null?" AND ATL2_METADATA.DATE_TIME > ADD_MONTHS(SYSDATE, -"+timeinterval+" ) ":" ")+
                (page!=null&&page>1?" AND ATL2_METADATA.MESS_ID <= :last_id ":" ")+
                "ORDER by ATL2_METADATA.MESS_ID DESC) "+ 
                "WHERE ROWNUM <=:num_of_rows) m,"+ 
                "ATL2_ATTRIBUTES, "+
                "ATL2_STRING2ID_ATTR_OPT "+
                "WHERE ATL2_ATTRIBUTES.LOGBOOK = M.LOGBOOK "+
                "AND M.MESS_ID=ATL2_ATTRIBUTES.MESS_ID "+ 
                "AND ATL2_ATTRIBUTES.ATTR_OPT = ATL2_STRING2ID_ATTR_OPT.ID "+ 
                "GROUP BY M.MESS_ID "+
                "ORDER by M.MESS_ID DESC) ATTRIBUTES "+
                "ON MESSAGE.MESS_ID = ATTRIBUTES.MESS_ID";

        log.info(sql);

        try {


            //Define parameters
            Map<String, Object> parameters = new HashMap<String, Object>();

            parameters.put("num_of_rows", numberOfMessages);

            //Compute first message ID for the desired page
            if(page!=null&&page>1) {

                //Note, this is the next ID, the last one is -1
                int lastMessageID = namedJdbcTemplate.queryForInt("select next_id from ATL2_MID_SEQUENCE where LOGBOOK= "+logbookID,new HashMap<String, Object>());
                lastMessageID=lastMessageID-1;

                //int lastMessageID= jdbcTemplate.queryForInt("select * from ( select MESS_ID from ATL2_METADATA  " +
                //        " where ATL2_METADATA.LOGBOOK = 70 ORDER BY MESS_ID DESC) where ROWNUM<=1" , new HashMap<String, Object>());
                parameters.put("last_id", lastMessageID - (page-1)*numberOfMessages);
            }

            long start=System.currentTimeMillis();
            List<Message> atls=namedJdbcTemplate.query(sql, parameters, new MessageMapper());
            
            
            /**
             * Setting attachments. How much it cost?
             */
            //for(Message m:atls) {
            //    //Get Attachments information
            //    List<Attachment> attachments = getAttachmentsMetadata(m.getID());
           //     if(!attachments.isEmpty())
            //        m.setAttachmentList(new AttachmentList(attachments));
           // }
            log.trace("Time elapsed to query DB and constructu objects: "+(System.currentTimeMillis()-start));
            return atls;

        }catch(EmptyResultDataAccessException erDAE){
            log.error(erDAE.getMessage());
            throw erDAE;
        }catch(DataAccessException eDAE){
            log.error(eDAE.getMessage());           
            throw eDAE;
        }       

    }

    private List<Message> getMessagesWithFiltering(QueryParams qp)  {
    	Integer defaultNumberOfMonthsToQuery=null;
        log.info("Querying messages with Criteria");

        //At this stage numberOfMessage may be null
        Integer numberOfMessages = qp.getNumberOfMessagesToReturn();
        if(numberOfMessages==null)
            numberOfMessages=DEFAULT_NUMBER_MESSAGES;

        Integer logbookID = qp.getLogbookID();
        if(logbookID==null)
        	logbookID=DEFAULT_LOGBOOK_ID;

        //BUILD SQL FILTER STRING
        String criteria_query = "";
        List<String> criteria = new LinkedList<String>();
        if(qp.hasFilterCriteria()) {

            //GET LIST OF CRITERIA
            if(qp.getAuthor()!=null)
                criteria.add(" LOWER(ATTRIBUTES.AUTHOR) LIKE LOWER('%"+qp.getAuthor()+"%') ");
            if(qp.getUsername()!=null)
                criteria.add("ATTRIBUTES.USERN='"+qp.getUsername()+"'");
            if(qp.getSystemAffected()!=null)
                criteria.add("REGEXP_LIKE(ATTRIBUTES.SYSTEM_AFFECTED, '.*"+qp.getSystemAffected()+".*')");
            if(qp.getMessageType()!=null)
                criteria.add("MESSAGE_TYPE='"+qp.getMessageType()+"'");
            if(qp.getSubject()!=null)
                criteria.add(" LOWER(ATTRIBUTES.SUBJECT) LIKE LOWER('%"+qp.getSubject()+"%') ");
            if(qp.getText()!=null)
                criteria.add(" LOWER(MESSAGE.BODYCLOB) LIKE LOWER('%"+qp.getText()+"%') ");
            if(qp.getRunNumber()!=null)
            	criteria.add(" MESSAGE_TYPE='DQSummary' AND upper(ATTRIBUTES.RUNNUMBER) LIKE '%"+qp.getRunNumber()+"%'");
            
            //Please note the data interval is handled directly in the SQL query below
            //BUG fix for default months interval that is filled in anyway
            if(qp.getStartDate()!=null || qp.getEndDate()!=null) defaultNumberOfMonthsToQuery=null;
            else defaultNumberOfMonthsToQuery=qp.getNumberOfMonthsToQuery();
            	
            if(!criteria.isEmpty()) {

                //BUILD SQL CONCATENATION
                StringBuilder sb = new StringBuilder();
                sb.append(" WHERE ");
                for(int i=0; i< criteria.size(); i++){
                    sb.append(criteria.get(i));
                    if(!(i+1==criteria.size()))
                        sb.append(" AND ");
                }
                criteria_query = sb.toString();
            }

        }

        String sql = "SELECT MESSAGE.MESS_ID, MESSAGE.DATE_TIME, ATTRIBUTES.AUTHOR, ATTRIBUTES.SUBJECT, " +
                "ATTRIBUTES.SYSTEM_AFFECTED, ATTRIBUTES.MESSAGE_TYPE,MESSAGE.THREAD_HEAD, MESSAGE.HAS_REPLIES, "+
                "MESSAGE.REPLY_TO, MESSAGE.HAS_ATTACHMENTS, ATTRIBUTES.STATUS, ATTRIBUTES.USERN, " +
                " MESSAGE.BODYCLOB "+
                "FROM "+ 
                "(SELECT  ATL2_METADATA.MESS_ID," +
                "ATL2_METADATA.DATE_TIME," +
                "ATL2_METADATA.THREAD_HEAD,"+ 
                "ATL2_METADATA.HAS_REPLIES,"+ 
                "ATL2_METADATA.REPLY_TO,"+ 
                "ATL2_METADATA.HAS_ATTACHMENTS,"+ 
                "ATL2_METADATA.BODY AS BODYCLOB "+
                "FROM   ATL2_METADATA ATL2_METADATA "+
                "WHERE  ATL2_METADATA.LOGBOOK = "+logbookID +
                (defaultNumberOfMonthsToQuery!=null?" AND ATL2_METADATA.DATE_TIME > ADD_MONTHS(SYSDATE, -"+defaultNumberOfMonthsToQuery+" ) ":"")+
                (qp.getStartDate()!=null?" AND  ATL2_METADATA.DATE_TIME >= '"+qp.getStartDate()+"' ":"")+
                (qp.getEndDate()!=null?" AND  ATL2_METADATA.DATE_TIME < '"+qp.getEndDate()+"' ":"")+
                //(qp.getLastID()!=null?" AND ATL2_METADATA.MESS_ID<"+qp.getLastID()+" ":"")+
                //(qp.getFirstID()!=null?" AND ATL2_METADATA.MESS_ID>"+qp.getFirstID()+" ":"")+

                "ORDER by ATL2_METADATA.MESS_ID DESC) MESSAGE "+
                "INNER JOIN "+
                "(SELECT M.MESS_ID,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 7, ATL2_ATTRIBUTES.ATTR_VALUE, NULL ) ) AS AUTHOR,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 5, ATL2_ATTRIBUTES.ATTR_VALUE, NULL ) ) AS USERN,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 23, ATL2_ATTRIBUTES.ATTR_VALUE, NULL ) ) AS SUBJECT,"+
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 12, ATL2_STRING2ID_ATTR_OPT.STR, NULL ) ) AS STATUS,"+
                "LISTAGG(decode(ATL2_ATTRIBUTES.ATTR_NAME, 14, ATL2_STRING2ID_ATTR_OPT.STR, NULL ) , \';\') "+ 
                "WITHIN GROUP (ORDER BY M.MESS_ID) AS SYSTEM_AFFECTED,"+ 
                "max( decode( ATL2_ATTRIBUTES.ATTR_NAME, 772, ATL2_STRING2ID_ATTR_OPT.STR, NULL ) ) AS MESSAGE_TYPE "+
                (qp.getRunNumber()!=null?", LISTAGG(decode(ATL2_ATTRIBUTES_VERBOSE.ATTR_NAME_STR,'RUNNUMBER',ATL2_ATTRIBUTES_VERBOSE.ATTR_VALUE, NULL) , ', ') WITHIN GROUP (ORDER BY M.MESS_ID) AS RUNNUMBER ":"")+
                
                " FROM "+
                "(SELECT  ATL2_METADATA.MESS_ID, ATL2_METADATA.LOGBOOK "+
                " FROM ATL2_METADATA ATL2_METADATA "+
                " WHERE ATL2_METADATA.LOGBOOK = "+ logbookID +
                (defaultNumberOfMonthsToQuery!=null?" AND ATL2_METADATA.DATE_TIME > ADD_MONTHS(SYSDATE, -"+defaultNumberOfMonthsToQuery+" ) ":"")+
                (qp.getStartDate()!=null?" AND  ATL2_METADATA.DATE_TIME >= '"+qp.getStartDate()+"' ":"")+
                (qp.getEndDate()!=null?" AND  ATL2_METADATA.DATE_TIME < '"+qp.getEndDate()+"' ":"")+
                //(qp.getLastID()!=null?" AND ATL2_METADATA.MESS_ID<"+qp.getLastID()+" ":"")+
                //(qp.getFirstID()!=null?" AND ATL2_METADATA.MESS_ID>"+qp.getFirstID()+" ":"")+
                "ORDER by ATL2_METADATA.MESS_ID DESC)  m,"+ 
                "ATL2_ATTRIBUTES, "+
                "ATL2_STRING2ID_ATTR_OPT "+
                (qp.getRunNumber()!=null?", ATL2_ATTRIBUTES_VERBOSE ":"")+
                "WHERE ATL2_ATTRIBUTES.LOGBOOK = M.LOGBOOK "+
                "AND M.MESS_ID=ATL2_ATTRIBUTES.MESS_ID "+ 
                "AND ATL2_ATTRIBUTES.ATTR_OPT = ATL2_STRING2ID_ATTR_OPT.ID "+ 
                (qp.getRunNumber()!=null?" AND ATL2_ATTRIBUTES_VERBOSE.MESS_ID=ATL2_ATTRIBUTES.MESS_ID AND ATL2_ATTRIBUTES_VERBOSE.ATTR_NAME=ATL2_ATTRIBUTES.ATTR_NAME AND ATL2_ATTRIBUTES_VERBOSE.LOGBOOK=ATL2_ATTRIBUTES.LOGBOOK AND ATL2_ATTRIBUTES_VERBOSE.ATTR_OPT = ATL2_STRING2ID_ATTR_OPT.ID ":"")+
                "GROUP BY M.MESS_ID "+
                "ORDER by M.MESS_ID DESC) ATTRIBUTES "+
                "ON MESSAGE.MESS_ID = ATTRIBUTES.MESS_ID "+
                criteria_query +
                (criteria_query.equals("")?" WHERE ROWNUM <= :num_of_rows":" AND ROWNUM <= :num_of_rows");

        log.info(sql);

        try {


            //Define parameters
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("num_of_rows", numberOfMessages);

            //Compute first message ID for the desired page
            //int firstIDPerPage = lastMessageID - (page==1?0:((page-1))*limit);
            //parameters.put("last_id", firstIDPerPage);
            //Page cannot be null
            //parameters.put("begin_pagination", page==1?0:((page-1)*numberOfMessages));
            //parameters.put("end_pagination", (page*numberOfMessages));
            //log.trace("begin " +parameters.get("begin_pagination")+ "end "+parameters.get("end_pagination"));


            long start=System.currentTimeMillis();
            List<Message> atls=namedJdbcTemplate.query(sql, parameters, new MessageMapper());
            log.trace("Time elapsed to query DB and build objects mapping: "+(System.currentTimeMillis()-start));
            return atls;

        }catch(EmptyResultDataAccessException erDAE){
            log.error(erDAE.getMessage());
            throw erDAE;
        }catch(DataAccessException eDAE){
            log.error(eDAE.getMessage());           
            throw eDAE;
        }       


    }



    @Override
    public String updateMessageTextAndDate(int logbookid, Long messageID, String text) {

        java.util.Date date = new java.util.Date();
        long t = date.getTime();
        java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);
        //log.trace(sqlTimestamp);
      
        namedJdbcTemplate.update("UPDATE atl2_metadata SET body = BODY || :TEXT, DATE_TIME = :DATE_TIME where MESS_ID = :ID AND LOGBOOK=:LOGBOOKID", 
                new MapSqlParameterSource().addValue("TEXT", text).addValue("ID", messageID).addValue("DATE_TIME",sqlTimestamp).addValue("LOGBOOKID",logbookid ));
        
        return getMessageBody(logbookid, messageID);
        
    }

    /*
     * From the spring doc:
     * http://static.springsource.org/spring/docs/3.0.x/spring-framework-reference/html/jdbc.html
     * 
     * (non-Javadoc)
     * @see elisa.rest.MessageDAO#addAttachment(java.lang.Long, java.io.InputStream, java.lang.String)
     */
    @Override
    public Attachment createAttachment(int logbookid, final Long messageID, 
            final InputStream blobIs,
            final long fileSize, 
             String originalFilename) {


        /**
         * Get last attachment ID (WHY NOT AUTO INCREMENT?)
         */

        //Please note, already incremented
        final Long attachmentID = jdbcTemplate.queryForLong("SELECT MAX(ATTACH_ID)+1 FROM ATL2_BLOB");
        //UPDATE TODO be atomic
        //jdbc.update("UPDATE ATL2_BLOB SET ATTACH_ID=ATTACH_ID + 1");
        
        final long blobid;

        /**
         * Create the new Blob entry in the ATLS_BLOB table
         */

        log.trace("Next blob id: "+attachmentID );
        final LobHandler lobHandler = new DefaultLobHandler();
        blobid = jdbcTemplate.execute(
                "INSERT INTO ATL2_BLOB (ATTACH_ID, DATA) VALUES (?, ?)",
                new AbstractLobCreatingPreparedStatementCallback(lobHandler) {                                                       
                    protected void setValues(PreparedStatement ps, LobCreator lobCreator) 
                            throws SQLException {
                        ps.setLong(1, attachmentID);
                        lobCreator.setBlobAsBinaryStream(ps, 2, blobIs, (int)fileSize);  
                    }
                }
                );
        try {
            blobIs.close();
        } catch (IOException e) {
            log.error("Error closing blob handler stream "+e.getMessage());
        }

        /**
         * Update Blob ID sequence 
         */
        String sql_update =
                "UPDATE ATL2_SEQUENCE SET NEXT_ID = "+ attachmentID+1;
        int count = jdbcTemplate.update(sql_update);
        
        
        /**
         * Original filename approach name-messageID-1.extension is removed to be compliant to EliSA web interface.
         * 
         */
        
//        /** 
//         * Add message ID to file name to avoid conflict
//         */
//        originalFilename = FilenameUtils.removeExtension(originalFilename)+"-"+messageID+"."+FilenameUtils.getExtension(originalFilename);
//        
//        /**
//         * 
//         */
//        Integer numberOfAttachmentsWithSameName = jdbcTemplate.queryForInt("SELECT COUNT(*) FROM ATL2_ATTACHMENTS WHERE MESS_ID = ? AND FILENAME LIKE ?",
//                new Object[]{messageID, FilenameUtils.removeExtension(originalFilename)+"%"});
//        
//        String filename;
//        
//        if(numberOfAttachmentsWithSameName>0) {
//            filename =  FilenameUtils.removeExtension(originalFilename)+"-"+
//                    (numberOfAttachmentsWithSameName)+"."+FilenameUtils.getExtension(originalFilename);
//        } else {
//            filename = originalFilename;
//        }
        
        String filename=nowTrimmed().concat(originalFilename);
        
        log.trace("Filename: "+filename);
        
        /**
         * Link the new blob entry to the message in the ATL2_ATTACHMENTS TABLE
         */
        namedJdbcTemplate.update("INSERT INTO ATL2_ATTACHMENTS (LOGBOOK, MESS_ID, ATTACH_ID, FILENAME, \"SIZE\")" +
        		" VALUES (:LOGBOOKID, :MESS_ID, :ATTACH_ID, :FILENAME,:SIZE)",
        		new MapSqlParameterSource().addValue("LOGBOOKID",logbookid).addValue("MESS_ID", messageID).addValue("ATTACH_ID", attachmentID).addValue("FILENAME", filename)
                .addValue("SIZE", fileSize)); 
        
        /**
         * Update HAS_ATTRIBUTE field of the message
         */
        namedJdbcTemplate.update("UPDATE ATL2_METADATA SET HAS_ATTACHMENTS = 1 WHERE MESS_ID = :MESS_ID AND LOGBOOK=:LOGBOOKID",
                new MapSqlParameterSource().addValue("MESS_ID", messageID).addValue("LOGBOOKID", logbookid)); 
        
       
        return new Attachment(attachmentID, filename, null);

    }


    @Override
    public List<Attachment> getAttachmentsMetadata(int logbookid, Long messageID) {
        
        List<Attachment> attachments = this.namedJdbcTemplate.query("SELECT ATTACH_ID, FILENAME FROM ATL2_ATTACHMENTS WHERE MESS_ID = :MESS_ID AND LOGBOOK=:LOGBOOKID", 
                new MapSqlParameterSource().addValue("MESS_ID", messageID).addValue("LOGBOOKID", logbookid), new AttachmentMetadataMapper());
      
        return attachments;
        
    }


    @Override
    public Attachment getAttachment(Long messageID, Long attachmentID) {

        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        String sql = " SELECT * FROM ATL2_BLOB  WHERE ATTACH_ID =  "+attachmentID;
        //BE CAREFUL HANDINGLING STREAM! JDBC CLOSE THE CONNECTION JUST AFTER THE QUERY, SO TRANSFORMATIONS
        // HAVE TO BE DONE INTO THE ROWMAPPER FUNCTION
        byte[] ba  = jdbc.queryForObject(sql, new FileMapper());
        String sqlForName = " SELECT FILENAME FROM ATL2_ATTACHMENTS  WHERE MESS_ID = "+messageID+" AND ATTACH_ID="+attachmentID;
        String fileName = jdbc.queryForObject(sqlForName, String.class);
        return new Attachment(attachmentID,fileName,ba);

    }
    
    @Override
    public Attachment getAttachmentbyName(Long messageID, String attachmentName) {
        
        NamedParameterJdbcTemplate jdbc = new NamedParameterJdbcTemplate(dataSource);
        
        log.trace("Searching for attachment "+attachmentName+" for message "+messageID);
        
        Attachment attachment = jdbc.queryForObject("SELECT ATTACH_ID, FILENAME FROM ATL2_ATTACHMENTS WHERE MESS_ID = :MESS_ID AND FILENAME = :FILENAME", 
                new MapSqlParameterSource().addValue("MESS_ID", messageID).addValue("FILENAME", attachmentName), new AttachmentMetadataMapper());
        
        String sql = " SELECT * FROM ATL2_BLOB  WHERE ATTACH_ID =  "+attachment.getID();
        
        //BE CAREFUL HANDINGLING STREAM! JDBC CLOSE THE CONNECTION JUST AFTER THE QUERY, SO TRANSFORMATIONS
        // HAVE TO BE DONE INTO THE ROWMAPPER FUNCTION
        
        JdbcTemplate simplejdbc = new JdbcTemplate();
        byte[] ba  = simplejdbc.queryForObject(sql, new FileMapper());
        
        attachment.setByteArray(ba);
        return attachment;
    }

    
    
    @Override
    public String getMessageBody(int logbookid, Long messageID) {
        NamedParameterJdbcTemplate jdbc = new NamedParameterJdbcTemplate(dataSource);
        String sql = "SELECT BODY FROM ATL2_METADATA WHERE LOGBOOK=:LOGBOOKID AND MESS_ID=:MESS_ID";
        return jdbc.queryForObject(sql, new MapSqlParameterSource().addValue("LOGBOOKID",logbookid).addValue("MESS_ID",messageID), String.class);
        
    }
    
    
    
    
    private static class  FileMapper implements ParameterizedRowMapper<byte[]>
    {
        public byte[] mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            try {
                return IOUtils.toByteArray(rs.getBinaryStream(2));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }
    }
    

    private static class MessageMapper implements RowMapper<Message> {

        LobHandler lobHandler = new DefaultLobHandler();


        public Message mapRow(ResultSet rs, int rowNum) throws SQLException {

            Message message=new Message();
            message.setID(rs.getLong(1));               //MESS_ID
            message.setDate(rs.getTimestamp(2));    // getDate(2));//DATE_TIME
            //log.trace("DATE "+rs.getTimestamp(2));
            message.setAuthor(rs.getString(3));         //AUTHOR
            message.setSubject(rs.getString(4));        //SUBJECT
            message.setSystemAffectedFromString(rs.getString(5)); //SYSTEM_AFFECTED
            message.setMessageType(rs.getString(6));    //MESSAGE_TYPE
            message.setThreadHead(rs.getLong(7));       //THREAD_HEAD
            message.setHasReplies(rs.getInt(8));        //HAS_REPLIES  
            message.setReplyTo(rs.getLong(9));          //REPLY_TO
            message.setHasAttachments(rs.getInt(10));   //HAS_ATTACHMENTS
            message.setStatus(rs.getString(11));        //STATUS
            message.setUsername(rs.getString(12));      //USERNAME
            //Clob body = rs.getClob(13);				//BODY
            //message.setBody(body==null?"-":lobHandler.getClobAsString(rs, 13));
            //         
            message.setBody( lobHandler.getClobAsString(rs, 13));
            //message.setBody(body==null?body:body.replace("\\*", "*").replace("\\+", "+").replace("\\-", "-")
            //      .replace("\\_", "_").replace("\\=", "="));          //BODY  
            return message;
        }
    }
    
    
    private static class AttachmentMetadataMapper implements RowMapper<Attachment> {

        public Attachment mapRow(ResultSet rs, int rowNum) throws SQLException {

            Attachment attachment =  new Attachment();
            attachment.setID(rs.getLong(1));
            attachment.setFileName(rs.getString(2));
         
            return attachment;
        }
    }
    
    /**
     * SELECT ATTR_NAME_STR, ATTR_OPT, ATTR_OPT_STR, ATTR_VALUE ...
     * 
     * @author lucamag
     *
     */
    private static class OptionMapper implements RowMapper<Option> {

        public Option mapRow(ResultSet rs, int rowNum) throws SQLException {

            Option option = new Option();
            option.setName(rs.getString(1));
            Long option_attr_id = rs.getLong(2);
            if(option_attr_id==0) //Means its a FREETEXT
                option.setValue(rs.getString(4));
            else
                option.setValue(rs.getString(3));
      
            return option;
        }
    }



    private List<Option> getOptions(Message m) {
        
        log.trace("Retrieving from DB additional attributes for message ID: "+m.getID());
        
        /**
         * This requires a lot of gymnastic:
         * - get list of optional attributes names per message type from configuration
         * - translate the names in IDS from the tl2string2is_attr_name table
         * - get the list of attributes  from the attributes table for the certain message ID
         */
      
        // GET LIST OF OPTIONAL ATTRIBUTE NAMES PER MESSAGE TYPE FROM CONFIGURATION (WTF???)
        List<OptionMetadata> optionsMetadata = configurationDAO.getOptionsMetadata(m.getMessageType());
        if(optionsMetadata.isEmpty())
            return null;
        else
            log.debug(optionsMetadata.size()+" options found from configuration for message type "+m.getMessageType());
        
        // GET LIST OF ATTR_NAME IDS from atl2string2is_attr_name for the list of names above 
        List<String> optionNames = new LinkedList<String>();
        for(OptionMetadata o : optionsMetadata )
            optionNames.add(o.getName().toUpperCase()); //REMEMBER THE UPPERCASE!!!
        
        log.debug("Option names: "+StringUtils.join(optionNames, ", "));
        
        if(optionNames.isEmpty())
            return null;
        
        List<Long> optionNameIDs = namedJdbcTemplate.queryForList("SELECT ID FROM ATL2_STRING2ID_ATTR_NAME WHERE STR IN (:names)", 
                new MapSqlParameterSource().addValue("names", optionNames),
                Long.class);
        
        if(optionNameIDs.isEmpty())
            return null;
        log.debug("Option ids: "+StringUtils.join(optionNameIDs, ", "));
        
             
        // GET from ATL2ATTRIBUTES THE LIST OF ATTRIBUTES options FOR MESS_ID MATCHING IDs above
        List<Option> options = namedJdbcTemplate.query("SELECT ATTR_NAME_STR, ATTR_OPT, ATTR_OPT_STR, ATTR_VALUE " +
                "FROM ATL2_ATTRIBUTES_VERBOSE WHERE LOGBOOK=:logbook AND MESS_ID =:mess_id AND  ATTR_NAME IN (:nameIDs) ", 
                new MapSqlParameterSource().addValue("logbook", m.getLogbook()).addValue("mess_id", m.getID()).addValue("nameIDs", optionNameIDs),
                new OptionMapper());
        
        
        log.debug("Evaluating inner options.");
        
        //For every Option search for innerOpions
        for(Option opt: options) {
            
            //Get list of inner option for the current options from configuration
            List<OptionMetadata> innerOptionsMetadata = configurationDAO.getSecondLevelOptionsMetadata(m.getMessageType(), opt.getName());
            if(innerOptionsMetadata==null||innerOptionsMetadata.isEmpty())
                continue;
            else
                log.debug(innerOptionsMetadata.size()+" inner options found from configuration for option "+opt.getName());
           
            // GET LIST OF ATTR_NAME IDS from atl2string2is_attr_name for the list of names above 
            List<String> innerOptionNames = new LinkedList<String>();
            for(OptionMetadata o : innerOptionsMetadata )
                innerOptionNames.add(o.getName().toUpperCase()); //REMEMBER THE UPPERCASE!!!
            log.debug("Inner Option names: "+StringUtils.join(innerOptionNames, ", "));
            
            if(innerOptionNames.isEmpty())
                return null;
            List<Long> innerOptionNameIDs = namedJdbcTemplate.queryForList("SELECT ID FROM ATL2_STRING2ID_ATTR_NAME WHERE STR IN (:names)", 
                    new MapSqlParameterSource().addValue("names", innerOptionNames),
                    Long.class);
            log.debug("Inner Option ids: "+StringUtils.join(innerOptionNameIDs, ", "));
            
            if(innerOptionNameIDs.isEmpty())
                return null;
            // GET from ATL2ATTRIBUTES THE LIST OF ATTRIBUTES options FOR MESS_ID MATCHING IDs above
            List<Option> innerOptions = namedJdbcTemplate.query("SELECT ATTR_NAME_STR, ATTR_OPT, ATTR_OPT_STR, ATTR_VALUE " +
                    "FROM ATL2_ATTRIBUTES_VERBOSE WHERE LOGBOOK=:logbook AND MESS_ID =:mess_id AND  ATTR_NAME IN (:nameIDs) ", 
                    new MapSqlParameterSource().addValue("logbook", m.getLogbook()).addValue("mess_id", m.getID()).addValue("nameIDs", innerOptionNameIDs),
                    new OptionMapper());
            log.debug("Inner Option values: "+StringUtils.join(innerOptions, ", "));
           
            /**
             * Ugly Workaround for uppercase option name
             */
            
            for (Option o: innerOptions) 
                for(OptionMetadata om: innerOptionsMetadata) 
                    if(om.getName().toUpperCase().equals(o.getName())) {
                        o.setName(om.getName());
                        continue;
                    }
            
            if(!innerOptionNameIDs.isEmpty())
                opt.setOptions(new OptionList(innerOptions));
        }
      
        /**
         * Ugly Workaround for uppercase option name
         */
        for (Option o: options) 
            for(OptionMetadata om: optionsMetadata) 
                if(om.getName().toUpperCase().equals(o.getName())) {
                    o.setName(om.getName());
                    continue;
                }
        return options;
        
    }


    @Override
    public void setHasReplies(int logbookid, long id) {
        /**
         * Update HAS_REPLIES field of the message
         */
        namedJdbcTemplate.update("UPDATE ATL2_METADATA SET HAS_REPLIES = 1 WHERE MESS_ID = :MESS_ID AND LOGBOOK=:LOGBOOKID",
                new MapSqlParameterSource().addValue("MESS_ID", id).addValue("LOGBOOKID", logbookid)); 
        
    }
    

    private String nowTrimmed() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_Trim);
        return sdf.format(cal.getTime());
      }
    

}
