package elisa.rest;

import java.io.InputStream;
import java.util.List;

import org.springframework.stereotype.Repository;

import elisa.rest.model.Attachment;
import elisa.rest.model.Message;
import elisa.rest.model.MessageBody;

@Repository //This assure the Exception translation from Spring
public interface MessageDAO {
    
    public Message getMessage(int logbookid, Long ID);
    
    public String getMessageBody(int logbookid, Long messageID);
    
    public Message createMessage(Message m);
    
    public void updateMessage(Long ID, Message m);
    
    public List<Message> findByQuery(QueryParams qp);

    void deleteMessage(Long ID);

    public String updateMessageTextAndDate(int logbookid, Long messageID, String text);

    public Attachment createAttachment(int logbookid, Long messageID, InputStream inputStream,
            long l, String originalFilename);

    public List<Attachment> getAttachmentsMetadata(int logbookid, Long messageID);

    public Attachment getAttachment(Long messageID, Long attachmentID);

    public Attachment getAttachmentbyName(Long messageID, String attachmentName);

    public void setHasReplies(int logbookid, long id);

   
    

}
