package elisa.web.exhandler;

import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.XmlAttribute;
 


public class MyMapEntryType {
 
   @XmlAttribute
   public String key; 
 
   @XmlValue
   public String value;
 
}