package elisa.web.exhandler;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name="error_report")
@XmlType

public class MapResult {
    
    @XmlJavaTypeAdapter(MyMapAdapter.class)
    public Map<String, String> error;
    
    private Map<String, String> getMap() {
        return error;
    }
    
    public void setMap(Map<String, String> map) {
        this.error = map;
    }

}
