package elisa.web;

/**
 * Simulated business-logic exception indicating a desired business entity or record cannot be found.
 */
public class UnknownResourceException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1754893546064844677L;

    public UnknownResourceException(String msg) {
        super(msg);
    }
}

