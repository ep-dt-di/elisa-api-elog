package elisa.web;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.util.UriComponentsBuilder;

import elisa.rest.ConfigurationDAO;
import elisa.rest.JdbcMessageDAO;
import elisa.rest.MessageDAO;
import elisa.rest.QueryParams;
import elisa.rest.model.Attachment;
import elisa.rest.model.AttachmentList;
import elisa.rest.model.InputMessage;
import elisa.rest.model.Message;
import elisa.rest.model.MessageBody;
import elisa.rest.model.MessageList;
import elisa.rest.model.MessageTypeList;
import elisa.rest.model.Option;
import elisa.rest.model.OptionList;
import elisa.rest.model.OptionMetadata;
import elisa.rest.model.OptionMetadataList;
import elisa.rest.model.SystemAffectedList;
import elisa.service.Email;
import elisa.service.EmailNotification;
import elisa.service.EmailUtils;
import elisa.service.Utils;
import elisa.web.exhandler.RestError;
import elisa.web.exhandler.RestExceptionHandler;
import elisa.web.interceptor.PrincipalInterceptor;

/**
 * This class is the Spring MVC @Controller for the {@link EliSA REST interface}.
 * It defines the service interface in terms of URI structures and parameters.
 * It interprets user input and transform it into a model that is presented to user in several format.
 * <p/>
 * Object returned in the model are handled by a {@link org.springframework.web.servlet.view.ContentNegotiatingViewResolver}, 
 * so multiple format (XML, JSON or HTML) are supported via HTTP "Accept" header specification or URL extension.
 * </p>
 * Objects in the model are annotated with JAXB annotation to define the marshaled XML structure.
 * </p>
 * If no XML or JSON is requested as return type, the {@link org.springframework.web.servlet.view.ContentNegotiatingViewResolver} is not used and 
 * the standard {@link org.springframework.web.servlet.view.InternalResourceViewResolver} kicks in, 
 * so the string returned by each method is interpreted by as the jsp page to be used to show result.
 * <p/>
 * Exception handling is defined by a customised implementation of the {@link RestExceptionHandler }
 * returning XML or JSON representation of error code and additional information. {@link RestError}
 * 
 * @author lmagnoni
 *
 */

@Controller
@RequestMapping("/")
public class APIController {

    private static final Log log = LogFactory.getLog(APIController.class);
    
    /**
     * Names of the servlet attributes with principal information
     * populated by the interceptor {@link PrincipalInterceptor }
     */
    private final static String FULL_NAME ="full-name";
    private final static String USERNAME ="username";

    private final static String DEFAULT_LOGBOOK_NAME ="ATLAS";
    
    //For the message update
    private static final String DATE_FORMAT = "HH:mm:ss";

    
    /**
     * The following beans are autowired by Spring
     * with the bean of the proper type from the
     * application context (controller-context.xml)
     * All autowired beans are mandatory by default
     */
    @Autowired
    private MessageDAO messageDAO;
    @Autowired
    private ConfigurationDAO configurationDAO;
    @Autowired 
    private Utils utils;
    @Autowired
    private EmailUtils emailUtils;
    @Autowired
    private Jaxb2Marshaller jaxb2; //Used for handling multipart  message creation
    
    @RequestMapping(value="/", method = RequestMethod.GET)
    public String index() {
        return "welcome";
    }
    
    /**
     * Introduce logbook name.
     * 
     * Return the message by ID. The ID is extracted from the URL path.
     * <p/>
     * In case the message is not found for the corresponding ID, the {@link UnknownResourceException} is thrown and is 
     * handled by the {@link RestExceptionHanlder}, returning formatted information about the error.
     * <p/>
     * @param the ID of the message, specified as part of the URL.
     *  The regexp on the path enforce the input as numerical value. More validation can be done with JSR-303 annotation.
     */
    @RequestMapping(value={"/messages/{messageID:[0-9]+}","/{logbookName}/messages/{messageID:[0-9]+}"}, method = RequestMethod.GET)
    public String getMessage(HttpServletRequest request,
            //@PathVariable Map<String, Object> pathVariablesMap,
            Model model) {
    	
        String logbookName=DEFAULT_LOGBOOK_NAME;
        Long messageID=null;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
        if (pathVariablesMap.containsKey("messageID")) {
    		messageID = Long.valueOf((String)pathVariablesMap.get("messageID")).longValue() ;
    		log.debug("mesageID="+messageID);
        }
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }
    	   	
        log.info("Request handler: getMessage from logbook "+logbookName );
        try {
            //Retrieve message from DB
            Message message = messageDAO.getMessage(configurationDAO.getLogbookID(logbookName),messageID);
            //Augment message information with attachment links, relative to request URL
            setAttachmentLinks(message, request);
            log.debug("Returning "+messageID+".");
            model.addAttribute("message", message);
            setAttachmentLinks(message, request);
            return "rest_message";
            
        } catch (DataAccessException e) {
            log.warn("Message with ID "+messageID+" not found.");
            log.trace("Exception: "+e);
            throw new UnknownResourceException("Message not found for ID "+messageID);
        }
      
    }

    /**
     * Introduce the logbook name when retrieving and creating resources
     *
     * <p>Return a list of messages, ordered by message ID, matching
     * the (optional) filter criteria specified. This method performs a look-up
     * on all the existing messages.
     * <p>
     * The filter criteria are expressed as HTTP query parameters. Space is represented as the + char. 
     * Filtering criteria supported:
     * <ul>
     * <li> username: the username of the creator of the elog entry(?username=lmagnoni)
     * <li> author: the full name of the creator of the elog entry (?author=Luca+Magnoni)
     * <li> system_affected: one or more system affected, separated by comma (?system_affected=Run+Control)
     * <li> message_type: the message type of the elog entry (?message_type=)
     * <li> subject: the subject (matched with SQL LIKE approach) (?subject=Summary)
     * <li> from: initial date (?from='26-JUL-12 12:00')
     * <li> to: end date (?to='1-AUG-12 12:30:00')
     * <li> text: free text search (matched with SQL LIKE approach) (?text=Test+for+system)
     * <li> runNumber: search by DQSummary:runNumber (?runNumber=1234)
     * </ul>
     *  
     * @param limit the max number of messages returned, optional
     * @param startID, the message ID of the first message returned
     */
    @RequestMapping(value={"/messages","/{logbookName}/messages"}, method = RequestMethod.GET)    
    public String getMessages(HttpServletRequest request, 
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "username", required = false) String username, 
            @RequestParam(value = "author", required = false) String author, 
            @RequestParam(value = "systems_affected", required = false) String systemAffected , 
            @RequestParam(value = "message_type", required = false) String messageType, 
            @RequestParam(value = "from", required = false) String startDate, 
            @RequestParam(value = "to", required = false) String endDate, 
            @RequestParam(value = "subject", required = false) String subject, 
            @RequestParam(value = "month_interval", required = false,  defaultValue = "3") Integer months,
            @RequestParam(value = "text", required = false) String text,
            @RequestParam(value = "runNumber", required = false) String runNumber,
            Model model){
        
        String logbookName=DEFAULT_LOGBOOK_NAME;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
        }
        }
        log.info("Request handler: getMessages from logbook="+logbookName );

        /**
         * This code is to building the reference to the next and previous page
         */
        
        String requestURI = request.getRequestURL().toString();
        
        if(requestURI.contains("atlasdaq")||requestURI.contains("pc-atd-elisa"))
            requestURI.replaceAll(":8080", "");
        if(requestURI.contains("atlasdaq"))
            requestURI.replaceAll("http", "https");
        
        if(request.getQueryString()!=null)
            requestURI=requestURI+"?"+request.getQueryString();
        

        UriComponentsBuilder ub = UriComponentsBuilder.fromHttpUrl(requestURI);

        //Elaborating pagination links
        String prev = null;
        String next = null;
        if (page!=null&&page>1) {
            prev = ub.replaceQueryParam("page", page-1).build().toUri().toString();
            
            if(requestURI.contains("atlasdaq")||requestURI.contains("pc-atd-elisa")) {
                 prev = prev.replaceAll(":8080", "");
            }
            if(requestURI.contains("atlasdaq")) {
                 prev = prev.replaceAll("http", "https");
            }
        
        }
        if(page!=null)
            next=ub.replaceQueryParam("page", page+1).build().toUri().toString();
        else 
            next=ub.replaceQueryParam("page", 2).build().toUri().toString();
        
        if(requestURI.contains("atlasdaq")||requestURI.contains("pc-atd-elisa")) {
            next = next.replaceAll(":8080", ""); 
        }
        if(requestURI.contains("atlasdaq")) {
            next = next.replaceAll("http", "https");
        }

        QueryParams qp = new QueryParams.Builder().author(author).username(username).systemAffected(systemAffected).messageType(messageType)
                    .startDate(startDate).endDate(endDate).subject(subject).numberOfMessagesToReturn(limit).page(page).numberOfMonthsToQuery(months)
                    .text(text).logbookID(configurationDAO.getLogbookID(logbookName)).runNumber(runNumber).build();

        MessageList messages = new MessageList(prev, next, messageDAO.findByQuery(qp));
        model.addAttribute("messages", messages);
       	
        
        return "rest_list";

    }
    

    /**
     * <p>Return a list of possible message types per logbook 
     * <p>
     * @param limit the max number of messages returned, optional
     * @param startID, the message ID of the first message returned
     */
    @RequestMapping(value={"/mt","/{logbookName}/mt"}, method = RequestMethod.GET)
    public String getMessageTypes(HttpServletRequest request,
            Model model){
        
        log.info("Request handler: getMessageTypes" );
        String logbookName=DEFAULT_LOGBOOK_NAME;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
        }
        }
        log.info("Request handler: getMTs from logbook="+logbookName );
        List<String> mt =  configurationDAO.getMessageTypes(this.configurationDAO.getLogbookID(logbookName));
        model.addAttribute("mt", new MessageTypeList(mt));
        return "rest_mt";

    }

    /**
     * Introduce the logbook name when retrieving and creating resources
     * <p>Return a list of possible sa per logbook. Default logbook is ATLAS
     * <p>
     */
    @RequestMapping(value={"/sa","/{logbookName}/sa"}, method = RequestMethod.GET)
    public String getSystemsAffected(HttpServletRequest request,
            Model model){
        log.info("Processing getSystemsAffected request: "+request.getRequestURL().toString());

        String logbookName=DEFAULT_LOGBOOK_NAME;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
        }
        }
        log.info("Request handler: getSA from logbook="+logbookName );

        List<String> sa = configurationDAO.getSystemAffected(this.configurationDAO.getLogbookID(logbookName));
        model.addAttribute("sa", new SystemAffectedList(sa));
        return "rest_sa";

    }

    /**
     * <p>Return a list of possible message types
     * <p>

     * @param limit the max number of messages returned, optional
     * @param startID, the message ID of the first message returned
     */
    @RequestMapping(value="/mt/{messageType}/sa", method = RequestMethod.GET)
    public String getSystemsAffectedPerMessageType(HttpServletRequest request, 
            @PathVariable String messageType, Model model){
        
        log.info("Request handler: getSystemsAffected per message type: " +messageType);

        try {
            List<String> sa = configurationDAO.getSystemAffected(messageType);
            if(sa.isEmpty())
                throw new EmptyResultDataAccessException(1);
    
            model.addAttribute("sa", new SystemAffectedList(sa));
            model.addAttribute("mt", messageType);
            
            log.info("Returning system affected list per message type "+messageType+"to view resolver.");
            
            return "rest_samt";
        
        } catch(EmptyResultDataAccessException erDAE){
            log.info("No system affected preset found for the message type: "+messageType);
            throw new UnknownResourceException("No system affected preset found for the message type: "+messageType);
        }catch(DataAccessException eDAE){
            log.warn("Invalid message type specified: "+messageType);
            throw new UnknownResourceException("Invalid message type specified: "+messageType);
        }
       

    }

    /**
     * <p>Return a list of possible message types
     * <p>

     * @param limit the max number of messages returned, optional
     * @param startID, the message ID of the first message returned
     */
    @RequestMapping(value="/mt/{messageType}/opt", method = RequestMethod.GET)
    public String getOptionsPerMessageType(HttpServletRequest request, 
            @PathVariable String messageType, Model model){
        
        log.info("Request handler: getOptions per message type: " +messageType);

        try {
            List<OptionMetadata> opts = configurationDAO.getOptionsMetadata(messageType);
            if(opts.isEmpty())
                throw new EmptyResultDataAccessException(1);
    
            model.addAttribute("opt", new OptionMetadataList(opts));
            model.addAttribute("mt", messageType);
            
            log.info("Returning option list for message type "+messageType+" to view resolver.");
            return "rest_opts";
            
        } catch(EmptyResultDataAccessException erDAE){
            log.info("No options found for the message type: "+messageType);
            throw new UnknownResourceException("No options found for the message type: "+messageType);
        }catch(DataAccessException eDAE){
            log.warn("Invalid message type specified: "+messageType);
            throw new UnknownResourceException("Invalid message type specified: "+messageType);
        }

       

    }



    /**
     * <p>Return a list of possible message types
     * <p>

     * @param limit the max number of messages returned, optional
     * @param startID, the message ID of the first message returned
     */

    @RequestMapping(value="/mt/{messageType}/opt/{optionName}", method = RequestMethod.GET)

    public String getOptionDetails(HttpServletRequest request, 
            @PathVariable String messageType, @PathVariable String optionName,  Model model){

        log.info("Request handler: getOptionsDetails per message type: " +messageType+" per option: "+optionName);

        OptionMetadata opt = configurationDAO.getOptionMetadata(messageType, optionName);

        model.addAttribute("opt", opt);
        model.addAttribute("mt", messageType);

        return "rest_opt";

    }

    /**
     * <p>Return a list of possible message types
     * <p>

     * @param limit the max number of messages returned, optional
     * @param startID, the message ID of the first message returned
     */

    @RequestMapping(value="/mt/{messageType}/opt/{optionName}/{value}", method = RequestMethod.GET)

    public String getOptionDetailsByValue(HttpServletRequest request, 
            @PathVariable String messageType, 
            @PathVariable String optionName, 
            @PathVariable("value") String optionValue, 
            Model model){
        

        log.info("Request handler: getOptionsDetailsByValue per message type: " +messageType+" - "+optionName+" = "+optionValue);

        List<OptionMetadata> opt = configurationDAO.getSecondLevelOptionMetadata(messageType, optionName, optionValue);

        model.addAttribute("opt", opt);
        model.addAttribute("mt", messageType);

        return "rest_opt";

    }
    

    /**
     * Introduce logbookname parameter
     * Return a representation of the text of the message identified by the ID.
     * <p/>
     * In case the message is not found for the corresponding ID, the {@link UnknownResourceException} is thrown and is 
     * handler by the {@link RestExceptionHanlder}, returning formatted information about the error.
     * <p/>
     * @param the ID of the message, specified as part of the URL.
     *  The regexp on the path enforce the input as numerical value. More validation can be done with JSR-303 annotation.
     */
    @RequestMapping(value={"/messages/{messageID:[0-9]+}/body","/{logbookName}/messages/{messageID:[0-9]+}/body"}, method = RequestMethod.GET)
    public String getMessageBody(HttpServletRequest request, 
           /* @PathVariable Long messageID,@PathVariable String logbookName,*/
            Model model) {
        
        log.info("Request handler: getMessageBody");
        
        String logbookName=DEFAULT_LOGBOOK_NAME;
        Long messageID=null;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
        if (pathVariablesMap.containsKey("messageID")) {
    		messageID = Long.valueOf((String)pathVariablesMap.get("messageID")).longValue() ;
    		log.debug("mesageID="+messageID);
        }
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }

        try {
            
            String text = messageDAO.getMessageBody(configurationDAO.getLogbookID(logbookName), messageID);
            MessageBody body = new MessageBody();
            body.setText(text);
            log.debug("Returning body for ID "+messageID+" to view processor.");
            model.addAttribute("message_body", body);
            model.addAttribute("message_id", messageID);

        } catch (DataAccessException e) {
            log.warn("Message with ID "+messageID+" not found.");
            log.trace("Exception: "+e);
            throw new UnknownResourceException("Message not found for ID "+messageID);
        }

        return "rest_message_text";
    }

    /**
     * Introduce logbook name
     * Update the text of the message identified by the ID with the data provided as input
     * <p/>
     * Input can be sent as XML or JSON document (specifying HTTP "Content-Type" header to the proper value).
     * <p/>
     * Input data is validated with JSR-303 annotation on the un-marshaled class {@link MessageBody}.
     * <p/>
     * Protection from SQL injection attack is handled by the {@link JdbcMessageDAO} leveraging on {@link JdbcTemplate} functionalities.
     * <p/>
     * In case the message is not found for the corresponding ID, the {@link UnknownResourceException} is thrown and is 
     * handler by the {@link RestExceptionHanlder}, returning formatted information about the error.
     * <p/>
     * Return a representation of the updated text of the message identified by the ID.
     * @param the only mandatory parameter is the ID of the message, specified as part of the URL
     */
    
    @ResponseStatus( HttpStatus.CREATED )
    @RequestMapping(value={"/messages/{messageID:[0-9]+}/body","/{logbookName}/messages/{messageID:[0-9]+}/body"}, method = RequestMethod.PUT)
    public String updateMessageBody(HttpServletRequest request, /*@PathVariable Long messageID, @PathVariable String logbookName,*/
            @Valid @RequestBody MessageBody text,
            Model model) {
        
        log.info("Request handler: updateMessageBody");

        String logbookName=DEFAULT_LOGBOOK_NAME;
        Long messageID=null;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
        if (pathVariablesMap.containsKey("messageID")) {
    		messageID = Long.valueOf((String)pathVariablesMap.get("messageID")).longValue() ;
    		log.debug("mesageID="+messageID);
        }
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }

        //Get principal information populated by interceptor
        String displayname= (String)request.getAttribute(FULL_NAME);
        String username=  (String)request.getAttribute(USERNAME);
        
		try {
            String messageAuthor=messageDAO.getMessage(configurationDAO.getLogbookID(logbookName), messageID).getAuthor();
            String updateText="";

            if(displayname.equals(messageAuthor)){
                updateText="<br/>["+now()+"]<br/>"+text.getText();
            }
            else {
                updateText="<br/>["+now()+"] Edited by "+displayname+" ("+username+")<br/>"+text.getText();
            }   
            String updateText2md=utils.cleanhtml2markdown(updateText);
            log.debug("Updating text");
            String newText = messageDAO.updateMessageTextAndDate(configurationDAO.getLogbookID(logbookName), messageID, updateText2md);
            
			Message message=messageDAO.getMessage(configurationDAO.getLogbookID(logbookName), messageID);
			setAttachmentLinks(message, request);
			emailUtils.createEmail(message);

            log.info("Message body updated for ID "+messageID+". Returning the updated full message.");
            
            model.addAttribute("message", message);
            return "rest_message";
            
        } catch (DataAccessException e) {
            log.warn("Message with ID "+messageID+" not found.");
            log.trace("Exception: "+e);
            throw new UnknownResourceException("Message not found for ID "+messageID);
        }       
    }
    
    
    
    /**
     * Introduce logbook name
     * not used
     */
    @ResponseStatus( HttpStatus.CREATED )
    @RequestMapping(value={"/messages/{messageID:[0-9]+}/body","/{logbookName}/messages/{messageID:[0-9]+}/body"},
    method = RequestMethod.POST,  
    consumes = {"multipart/form-data", "multipart/mixed"})
    public String updateMessageBodyWithAttachments(HttpServletRequest request,
            /*@PathVariable Long messageID, @PathVariable String logbookName, */
            @Valid @RequestPart("body") MessageBody text,
            Model model) {
        
        log.info("Request handler: updateMessageBodyWithAttachments");

        String logbookName=DEFAULT_LOGBOOK_NAME;
        Long messageID=null;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
        if (pathVariablesMap.containsKey("messageID")) {
    		messageID = Long.valueOf((String)pathVariablesMap.get("messageID")).longValue() ;
    		log.debug("mesageID="+messageID);
        }
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }
        
        
        //Get principal information populated by interceptor
        String displayname= (String)request.getAttribute(FULL_NAME);
        String username=  (String)request.getAttribute(USERNAME);
        
        try {
            String messageAuthor=messageDAO.getMessage(configurationDAO.getLogbookID(logbookName), messageID).getAuthor();
            String updateText="";
            if(displayname.equals(messageAuthor)){
                updateText="<br/>["+now()+"]<br/>"+text.getText();
            }
            else {
                updateText="<br/>["+now()+"] Edited by "+displayname+" ("+username+")<br/>"+text.getText();
            }   
            String updateText2md=utils.cleanhtml2markdown(updateText);
            
            String newText = messageDAO.updateMessageTextAndDate(configurationDAO.getLogbookID(logbookName), messageID, updateText2md);

            log.info("Message body updated for ID "+messageID+". Returning new body to view processor.");
            
           
            handleMultipartAttachments(logbookName,(MultipartHttpServletRequest) request, messageID);

            Message msg=messageDAO.getMessage(configurationDAO.getLogbookID(logbookName), messageID);
            setAttachmentLinks(msg, request);
            
            emailUtils.createEmail(msg);
           
            model.addAttribute("message", msg);
            return "rest_message";
            
        } catch (DataAccessException e) {
            log.warn("Message with ID "+messageID+" not found.");
            log.trace("Exception: "+e);
            throw new UnknownResourceException("Message not found for ID "+messageID);
        }

        
    }
    
    /**
     * Introduce logbook name
     * Return a representation of the list of the attachment for the  message identified by the ID.
     * <p/>
     * In case the message is not found for the corresponding ID, the {@link UnknownResourceException} is thrown and is 
     * handler by the {@link RestExceptionHanlder}, returning formatted information about the error.
     * <p/>
     * @param the ID of the message, specified as part of the URL.
     *  The regexp on the path enforce the input as numerical value. More validation can be done with JSR-303 annotation.
     */
    @RequestMapping(value={"/messages/{messageID:[0-9]+}/attachments","/{logbookName}/messages/{messageID:[0-9]+}/attachments"}, method = RequestMethod.GET)
    public String getAttachmentList(HttpServletRequest request, 
            /*@PathVariable Long messageID, @PathVariable String logbookName,*/
            Model model) {
        
        log.info("Request handler: getAttachmentsList");

        String logbookName=DEFAULT_LOGBOOK_NAME;
        Long messageID=null;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
        if (pathVariablesMap.containsKey("messageID")) {
    		messageID = Long.valueOf((String)pathVariablesMap.get("messageID")).longValue() ;
    		log.debug("mesageID="+messageID);
        }
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }

        try {
            List<Attachment> attachments = messageDAO.getAttachmentsMetadata(configurationDAO.getLogbookID(logbookName), messageID);
            //Set link
            for(Attachment a:attachments)
                a.setLink(request.getRequestURL().toString().replace(".xml", "").replace(".json", "")+"/"+a.getID());
            
            log.info(attachments.size()+" attachments retrieved for message "+messageID+". Returning results to view processor.");
            
            model.addAttribute("attachments", new AttachmentList(attachments));
            model.addAttribute("message_id", messageID);
            return "rest_attachments";

        } catch (DataAccessException e) {
            log.warn("Message not found for ID:"+messageID);
            log.trace("Exception: "+e);
            throw new UnknownResourceException("Message not found for ID "+messageID);
        }

       
    }

    /**
     * Introduce logbook name
     * Return the attachment identified by ID for the specified message ID.
     * <p/>
     * Levraging on Spring 3.1, {@link ResponseEntity<T>} is preferred to @ResponseBody 
     * in order to directly setting HTTP response header, without using Resolver.
     * <p/>
     * MimeType is established from filename and file content using Apache Tika library
     * <p/>
     * In case the message or attachment are not found for the corresponding ID, the {@link UnknownResourceException} is thrown and is 
     * handler by the {@link RestExceptionHanlder}, returning formatted information about the error.
     * <p/>
     * @param the ID of the message, specified as part of the URL.
     * @param the ID of the attachment, specified as part of the URL.
     *  
     *  The regexp on the path enforce the input as numerical value. More validation can be done with JSR-303 annotation.
     *  
     */
    
    @RequestMapping(value={"/messages/{messageID:[0-9]+}/attachments/{attachmentID:[0-9]+}","/{logbookName}/messages/{messageID:[0-9]+}/attachments/{attachmentID:[0-9]+}"}, method = RequestMethod.GET)
    public ResponseEntity<byte[]> getAttachment(HttpServletRequest request, 
            /*@PathVariable Long messageID,
            @PathVariable Long attachmentID, @PathVariable String logbookName, */
            HttpServletResponse response) {
        
        log.info("Request handler: getAttachment");

        String logbookName=DEFAULT_LOGBOOK_NAME;
        Long messageID=null;
        Long attachmentID=null;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
        if (pathVariablesMap.containsKey("messageID")) {
    		messageID = Long.valueOf((String)pathVariablesMap.get("messageID")).longValue() ;
    		log.debug("mesageID="+messageID);
        }
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
    	if (pathVariablesMap.containsKey("attachmentID")) {
    		attachmentID=Long.valueOf((String)pathVariablesMap.get("attachmentID")).longValue() ;
        }
        }
        
        try {
            Attachment attachment = messageDAO.getAttachment(messageID, attachmentID);
            HttpHeaders responseHeaders = new HttpHeaders();
    
            //Get MimeType with Apache TIKA
            String s;
            Tika tika = new Tika();
      
            s = tika.detect(attachment.getFileName());
            if(s==null)
                s=tika.detect(new ByteArrayInputStream(attachment.getByteArray()));
            
            responseHeaders.setContentType(MediaType.valueOf(s));
            
            log.info("Attachment "+attachmentID+" of type "+s+" retrieved for message "+messageID+". Returning as response body.");
            //responseHeaders.set("Content-Disposition", "attachment; filename=\""+attachment.getFileName()+"\"");
            return new ResponseEntity<byte[]>(attachment.getByteArray(), responseHeaders, HttpStatus.FOUND);
            
        } catch (DataAccessException e) {
            log.error("Attachments not found for message: "+messageID+".");
            throw new UnknownResourceException("Attachments not found for message: "+messageID+".");
        } catch (IOException e) {
            log.error("Error handling attachments for message: "+messageID+". "+e);
            throw new UnknownResourceException("Error handling attachments for message: "+messageID+". "+e.getMessage());
        }
    }

    /**
     * Introduce logbook name
     * Create one (or many) new attachment for the message identified by the ID.
     * <p/>
     * In case the message is not found for the corresponding ID, the {@link UnknownResourceException} is thrown and is 
     * handler by the {@link RestExceptionHanlder}, returning formatted information about the error.
     * <p/>
     * The Controller is using the new MultiPartResolver from Spring 3.1
     * The expected request should be:
     * 
     * Content-Type: multipart/form-data
     * 
     * Content-Disposition: form-data; name="file"
     * Content-Type: whatever the file is
     * body...
     * 
     * More info: http://static.springsource.org/spring/docs/current/spring-framework-reference/html/mvc.html#mvc-multipart
     * <p/>
     * @param the only mandatory parameter is the ID of the message, specified as part of the URL
     * 
     */
   
    @RequestMapping(value={"/messages/{messageID:[0-9]+}/attachments","/{logbookName}/messages/{messageID:[0-9]+}/attachments"},
            method = RequestMethod.POST, consumes = {"multipart/form-data", "multipart/mixed"})
    @ResponseStatus( HttpStatus.CREATED )
    public String createAttachments(HttpServletRequest request, 
            HttpServletResponse response, 
            /*@PathVariable Long messageID, @PathVariable String logbookName, */
            Model model) {
        
        log.info("Request handler: createAttachments");
        String logbookName=DEFAULT_LOGBOOK_NAME;
        Long messageID=null;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
        if (pathVariablesMap.containsKey("messageID")) {
    		messageID = Long.valueOf((String)pathVariablesMap.get("messageID")).longValue() ;
    		log.debug("mesageID="+messageID);
        }
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }
      
        try {
            List<Attachment> as = this.handleMultipartAttachments(logbookName,(MultipartHttpServletRequest)request, messageID);
            log.info(as.size()+" attachment saved for message " +messageID +".");
            //Set Location if a single attachment is created, otherwise to generic attachments url
            response.setHeader("Location", request.getRequestURL()+(as.size()==1?("/"+as.get(0).getID()):""));
            model.addAttribute("message_id", messageID);
            model.addAttribute("attachments", new AttachmentList(as));
            return "rest_attachments";
            
        } catch (DataAccessException e) {
            log.warn("Message not found for ID: "+messageID+". ");
            log.trace("Exception: "+e);
            throw new UnknownResourceException("Message not found for ID: "+messageID+". ");
        }
        
    }

    /**
     * introduce logbook name
     * Create a new message with user input.
     * <p/>
     * User can send an XML or JSON document as input (specifying HTTP "Content-Type" header to the proper value).
     * Input data is validated with JSR-303 annotation on the un-marshaled class {@link InputMessage}.
     * </p>
     * Input data defines a subset of the message properties. Server side message properties (such as user, host_ip, etc.) are
     * generated with information from HTTP request.
     * </p>
     * Return a complete representation of the new message in the specified format (following HTTP "Accept" header value ), 
     * or the error information in case of problem.
     * 
     * @return a representation of the newly created messages, or the error information.
     */

    @RequestMapping(value = {"/messages","/{logbookName}/messages"}, method = RequestMethod.POST)
    @ResponseStatus( HttpStatus.CREATED )
    public String createMessage(HttpServletRequest request,
            HttpServletResponse response,/* @PathVariable String logbookName,*/
            @Valid @RequestBody InputMessage im,
            Model model) {
        
        log.info("Request handler: createMessage");

        String logbookName=DEFAULT_LOGBOOK_NAME;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }
        
        Message new_message = insertMessage(logbookName, im, request);
        //log.debug("Message "+message.getID());
        //log.debug("New message created with ID: "+new_message.getID() +" "+new_message.getLogbook());

        setAttachmentLinks(new_message, request);
        
        emailUtils.createEmail(new_message);
		
		//Set Location
        response.setHeader("Location", request.getRequestURL()+"/"+new_message.getID());

        model.addAttribute("message", new_message);
        
        //log.debug("System affected list: "+new_message.getSystemAffectedAsString());
        return "rest_message";
    }
    
    /**
     * Introduce logbook name
     * This function handles the creation of a new messages with an
     * arbitrary number of attachments. More info:
     * http://static.springsource.org/spring/docs/current/spring-framework-reference/html/mvc.html#mvc-multipart
     * 
     * It receives a mixed multipart request with a XML or JSON formatted section and one or more
     * files. JSR 303 annotation applies for data validation.
     * 
     * The message part is handled by the standard HttpMessageConverter 
     * taking into consideration the 'Content-Type' header of the multipart. 
     * 
     * The files section are retrieved directly from the request.
     *  
     * 
     */
    @RequestMapping(value = {"/messages","/{logbookName}/messages"}, 
            method = RequestMethod.POST, 
            consumes = {"multipart/form-data", "multipart/mixed"})
    @ResponseStatus( HttpStatus.CREATED )
    public String createMessageWithAttachments(HttpServletRequest request, 
            HttpServletResponse response, /*@PathVariable String logbookName,*/
            @Valid @RequestPart("message") InputMessage message,
            Model model) {
        
        log.info("Request handler: createMessageWithAttachments");

        String logbookName=DEFAULT_LOGBOOK_NAME;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }
      
        Message new_message = insertMessage(logbookName, message, request);
        log.debug("New message created with ID: "+new_message.getID() +" "+new_message.getAuthor());
        
        handleMultipartAttachments(logbookName,(MultipartHttpServletRequest)request, new_message.getID());
       
        /***** Post Processing *********/

        //Read the message again to get attachments information
        new_message = messageDAO.getMessage(configurationDAO.getLogbookID(logbookName), new_message.getID());
        setAttachmentLinks(new_message, request);
        
        emailUtils.createEmail(new_message);

        //Set Location
        response.setHeader("Location", request.getRequestURL()+"/"+new_message.getID());
        /*******************************/
        
        model.addAttribute("message", new_message);
        return "rest_message";
    }

    /**
     * Introduce logbook name
     * Create a new message with user input (reply to a message)
     * <p/>
     * User can send an XML or JSON document as input (specifying HTTP "Content-Type" header to the proper value).
     * Input data is validated with JSR-303 annotation on the un-marshaled class {@link InputMessage}.
     * </p>
     * Input data defines a subset of the message properties. Server side message properties (such as user, host_ip, etc.) are
     * generated with information from HTTP request.
     * </p>
     * Return a complete representation of the new message in the specified format (following HTTP "Accept" header value ), 
     * or the error information in case of problem.
     * 
     * @return a representation of the newly created messages, or the error information.
     */

    @RequestMapping(value = {"/messages/{replyToMessageID:[0-9]+}","/{logbookName}/messages/{replyToMessageID:[0-9]+}"}, method = RequestMethod.POST)
    @ResponseStatus( HttpStatus.CREATED )
    public String replyMessage(HttpServletRequest request,
            HttpServletResponse response,
            /*@PathVariable Long replyToMessageID, @PathVariable String logbookName,*/
            @Valid @RequestBody InputMessage im,
            Model model) {
        
        log.info("Request handler: replyMessage");

        String logbookName=DEFAULT_LOGBOOK_NAME;
        Long replyToMessageID=null;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
        if (pathVariablesMap.containsKey("replyToMessageID")) {
    		replyToMessageID = Long.valueOf((String)pathVariablesMap.get("replyToMessageID")).longValue() ;
    		log.debug("replyToMessageID="+replyToMessageID);
        }
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }
        
        Message new_message = insertMessage(logbookName, im, replyToMessageID, request);
        //log.debug("Message "+message.getID());
        log.debug("Reply message created with ID: "+new_message.getID());
        setAttachmentLinks(new_message, request);
        
        
        emailUtils.createEmail(new_message);     
            
        //Set Location
        response.setHeader("Location", request.getRequestURL()+"/"+new_message.getID());

        model.addAttribute("message", new_message);
        
        return "rest_message";
    }
        
    /**
     * introduce logbook name
     * Message reply with attachment using multipart
     */
  
    @RequestMapping(value = {"/messages/{replyToMessageID:[0-9]+}","/{logbookName}/messages/{replyToMessageID:[0-9]+}"}, 
            method = RequestMethod.POST, consumes = {"multipart/form-data", "multipart/mixed"})
    @ResponseStatus( HttpStatus.CREATED )
    public String replyMessageWithAttachments(HttpServletRequest request,
            HttpServletResponse response,
            /*@PathVariable Long replyToMessageID, @PathVariable String logbookName,*/
            @Valid @RequestPart("message") InputMessage message,
            Model model) {
        
        log.info("Request handler: replyMessageWithAttachments");

        String logbookName=DEFAULT_LOGBOOK_NAME;
        Long replyToMessageID=null;
        Map<String, String> pathVariablesMap = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if(pathVariablesMap != null){
        if (pathVariablesMap.containsKey("replyToMessageID")) {
    		replyToMessageID = Long.valueOf((String)pathVariablesMap.get("replyToMessageID")).longValue() ;
    		log.debug("replyToMessageID="+replyToMessageID);
        }
    	if (pathVariablesMap.containsKey("logbookName")) {
    		logbookName=(String) pathVariablesMap.get("logbookName");
    		log.debug("logbook="+logbookName);
        }
        }
      
        Message new_message = insertMessage(logbookName, message, replyToMessageID, request);
        log.debug("Reply message created with ID: "+new_message.getID());
        
        this.handleMultipartAttachments(logbookName,(MultipartHttpServletRequest)request, new_message.getID());
        
        //Read the message again to get attachments information
        new_message = messageDAO.getMessage(configurationDAO.getLogbookID(logbookName), new_message.getID());
        setAttachmentLinks(new_message, request);
        
        emailUtils.createEmail(new_message);

        //Set Location
        response.setHeader("Location", request.getRequestURL()+"/"+new_message.getID());

        model.addAttribute("message", new_message);
      
        return "rest_message";
    }
    
    
  
    
    /************************
     * Utility methods
     ***********************/
    

    /**
     * Extract the multipart files from a multipart request and
     * insert them as attachments for the specifies message.
     * 
     * @param mrequest
     * @param messageID
     */
    private List<Attachment> handleMultipartAttachments(String logbookName, MultipartHttpServletRequest mrequest, Long messageID) {
        
        LinkedList<Attachment> attachments = new LinkedList<Attachment>();
        
        final Map<String, MultipartFile> fileMap = mrequest.getFileMap();
        for(String paramName:fileMap.keySet()) {
            Object data = fileMap.get(paramName);
            if (!(data instanceof MultipartFile)) {
                log.warn("Invalid attachment send by user.");
                throw new elisa.web.InvalidResourceException("Invalid multipart  section found with name: "+paramName);
            }
            MultipartFile file = (MultipartFile)  data;
            try {
                //Creating attachment
                Attachment attachment = messageDAO.createAttachment(configurationDAO.getLogbookID(logbookName), messageID, file.getInputStream(), file.getSize(), file.getOriginalFilename());
                log.info("Attachment created in logbook: "+logbookName+ " for message " +messageID +" with ID "+attachment.getID());
                attachments.add(attachment);
            } catch (IOException e) {
                log.error("Problem creating attachment."+e);
            }
        }
        
        return attachments;
    }

    /**
     * Populate the message attachment with absoulte links
     * derived from sever URL
     * @param message
     * @param request
     */
    private void setAttachmentLinks(Message message, HttpServletRequest request) {
        //Setting relative links
        if(message.getHasAttachments()==1&&message.getAttachments()!=null)
          for(Attachment a:message.getAttachments().getList())
                a.setLink(request.getRequestURL().toString().replace(".xml", "").replace(".json", "")+"/attachments/"+a.getID());
    }
    
    
    private Message insertMessage(String logbookName, InputMessage inputMessage,HttpServletRequest request) {
        return this.insertMessage(logbookName, inputMessage, null, request);
    }
    
    /**
     * Create a new message, both as new or reply to existing one.
     * 
     * @param inputMessage
     * @param messageToReply
     * @param request
     * @return
     */
    private Message insertMessage(String logbookName, InputMessage inputMessage, Long messageToReply, HttpServletRequest request) {
        
        
        log.debug("Input message: "+inputMessage);

        //Get principal information populated by interceptor
        String displayname= (String)request.getAttribute(FULL_NAME);
        String username=  (String)request.getAttribute(USERNAME);
        
        Message replyTo = null;
        
        log.debug("Username: "+username+", Full name: "+displayname);

        /**
         * Set up server side parameters
         */     
        Message m = new Message();        
        m.setAuthor((inputMessage.getAuthor()==null||inputMessage.getAuthor().isEmpty())?displayname:inputMessage.getAuthor());
        if(m.getAuthor().equals("(null)")){
             throw new InvalidResourceException("The author value: "+displayname +" is not valid.");
        }
        
        if(username.equals("(null)") == false){
        	m.setUsername(username);
        }
        else {
            throw new InvalidResourceException("The username value: "+username +" value is not valid.");       	
        }
        m.setHost_ip(request.getRemoteAddr());
        java.util.Date date = new java.util.Date();
        
        long t = date.getTime();
        java.sql.Date sqlDate = new java.sql.Date(t);
        java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(t);
        log.debug(sqlTimestamp);
        
        m.setDate(sqlTimestamp);
        
        /**
         * Set Read only parameter. TO BE ENFORCED BY DESIGN
         */
        //log.debug("logbookName="+logbookName+" logbookId="+configurationDAO.getLogbookID(logbookName));
        m.setLogbook(configurationDAO.getLogbookID(logbookName));
        
        m.setHasReplies(0);
        m.setReplyTo(0);
        m.setHasAttachments(0);
        m.setEncoding(3);
        
        /**
         * Get fields from Input Message
         */
        if(inputMessage.getBody()!=null)
           	m.setBody(inputMessage.getBody());
        if(inputMessage.getSubject()!=null && !inputMessage.getMessageType().equals("fake-notnull"))
           	m.setSubject(inputMessage.getSubject());
        if(inputMessage.getMessageType()!=null && !inputMessage.getMessageType().equals("fake-notnull")) {
           	//Do not accept anymore new entries with DCS message type; so there is no need to remove it from the DB 
        	if(!inputMessage.getMessageType().equals("DCS")) {
           		m.setMessageType(inputMessage.getMessageType());
           	}
           	else {
           		throw new InvalidResourceException("The message type specified: "+inputMessage.getMessageType() +" is not valid anymore. Use Default Message Type instead.");
           	}
        }   	
        if(inputMessage.getSystemsAffected()!=null)
            m.setSystemAffected(inputMessage.getSystemsAffected());
        if(inputMessage.getStatus()!=null)
            m.setStatus(inputMessage.getStatus());
        
        //Setting additional attributes
        
        if(inputMessage.getOptions()!=null&&!inputMessage.getOptions().getList().isEmpty()) {
            m.setOptionalAttributes(inputMessage.getOptions().getList());
            
            //Retrieve Option Metadata from ConfigurationDB for every option and inner options
            try {
                for(Option o:m.getOptionalAttributes().getList()) {
                	//log.debug("o "+o.getName()+ o.getValue());               	
                    o.setOptionMetadata(configurationDAO.getOptionMetadata(m.getMessageType(), o.getName()));
                    if(o.getOptions()!=null)
                        for(Option l2o:o.getOptions().getList()) {
                        	//log.debug("l2o "+l2o.getName()+ l2o.getValue());
                        	List<OptionMetadata> ll2o =configurationDAO.getSecondLevelOptionMetadata(m.getMessageType(), o.getName(), o.getValue());
                        	for(OptionMetadata om:ll2o)
                        	   l2o.setOptionMetadata(om);
                        } 
                }
            }catch(DataAccessException eDAE){
                log.trace(eDAE.getMessage());           
                throw new InvalidResourceException("Invalid optional attributes specified for message type "+m.getMessageType());
            }
        
        }
        
        /**
         * If the message is created as a reply to another one
         */
        
        //Get replied messages
        if(messageToReply!=null) 
            //Retrieve replyTo message
            replyTo = messageDAO.getMessage(configurationDAO.getLogbookID(logbookName), messageToReply);
        
        //Set fields
        if(messageToReply!=null) {
            //Check if there is no message type provided or is the "fake-notnull" string from Elisa mailer
            if(inputMessage.getMessageType()==null || inputMessage.getMessageType().equals("fake-notnull"))
               	//Do not accept anymore new entries with DCS message type; so there is no need to remove it from the DB 
            	if(!inputMessage.getMessageType().equals("DCS")) {
               		m.setMessageType(replyTo.getMessageType());
               	}
               	else {
               		throw new InvalidResourceException("The message type specified: "+inputMessage.getMessageType() +" is not valid anymore. Use Default Message Type instead.");
               	}
            if(!m.getMessageType().equals(replyTo.getMessageType()))
               throw new InvalidResourceException("The message type of the reply message: "+m.getMessageType() +" is not consistent with the original one: "+replyTo.getMessageType());
                
            //Retrieve replyTo message
            m.setThreadHead(replyTo.getThreadHead());
            m.setReplyTo(replyTo.getID());
            if(m.getStatus()==null)
            	m.setStatus(replyTo.getStatus());
            //Check if there is no subject provided or is the "fake-notnull" string  
            if(m.getSubject()==null || inputMessage.getMessageType().equals("fake-notnull"))
                m.setSubject("RE: "+replyTo.getSubject());

            //recreate the same text as for the replied message via the Web interface
            //String bb=m.getBody().concat("\nOn ").concat(replyTo.getDate().toString()).concat(", ").concat(replyTo.getAuthor()).concat(" wrote: \n");
            //m.setBody(bb.concat(replyTo.getBody()));
 
            //Inherit from replied message
            if((inputMessage.getOptions()==null)&&(replyTo.getOptionalAttributes()!=null)) {
            
                m.setOptionalAttributes(replyTo.getOptionalAttributes().getList());
                
                //Retrieve Option Metadata from ConfigurationDB for every option and inner options
                try {
                    for(Option o:m.getOptionalAttributes().getList()) {
                    	//log.debug("o "+o.getName()+ o.getValue());               	
                        o.setOptionMetadata(configurationDAO.getOptionMetadata(m.getMessageType(), o.getName()));
                        if(o.getOptions()!=null)
                            for(Option l2o:o.getOptions().getList()) {
                            	//log.debug("l2o "+l2o.getName()+ l2o.getValue());
                            	List<OptionMetadata> ll2o =configurationDAO.getSecondLevelOptionMetadata(m.getMessageType(), o.getName(), o.getValue());
                            	for(OptionMetadata om:ll2o)
                            	   l2o.setOptionMetadata(om);
                            } 
                    }
                }catch(DataAccessException eDAE){
                    log.trace(eDAE.getMessage());           
                    throw new InvalidResourceException("Invalid optional attributes specified for message type "+m.getMessageType());
                }
            }
            
            if(inputMessage.getSystemsAffected()==null)
                m.setSystemAffected(replyTo.getSystemAffected().getList());
            
        }
        
        Message new_message = messageDAO.createMessage(m);

        //Set has reply
        if(messageToReply!=null) {
            //Retrieve replyTo message
            messageDAO.setHasReplies(configurationDAO.getLogbookID(logbookName), replyTo.getID());
        }
        
        return new_message;        
    }
    
    private String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        return sdf.format(cal.getTime());
    }

}
