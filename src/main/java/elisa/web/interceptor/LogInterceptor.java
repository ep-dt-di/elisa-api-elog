package elisa.web.interceptor;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class LogInterceptor extends HandlerInterceptorAdapter {

    private static final Log log = LogFactory.getLog(LogInterceptor.class);

    //before the actual handler will be executed
    @Override
    public boolean preHandle(HttpServletRequest request, 
            HttpServletResponse response, Object handler)
                    throws Exception {

        long startTime = System.currentTimeMillis();
        request.setAttribute("startTime", startTime);

        logRequest(request);

        return true;
    }
    
    //after the handler is executed
    @Override
    public void postHandle(
        HttpServletRequest request, HttpServletResponse response, 
        Object handler, ModelAndView modelAndView)
        throws Exception {
 
        long startTime = (Long)request.getAttribute("startTime");
 
        long endTime = System.currentTimeMillis();
 
        long executeTime = endTime - startTime;
 
        //modified the exisitng modelAndView
        if(modelAndView!=null) //such as when retuning an attachment as byte[]
            modelAndView.addObject("executeTime",executeTime);
 
        log.info("Request processed. ExecuteTime : " + executeTime + "ms");
    }


    private void logRequest(HttpServletRequest request) {

        log.info(request.getMethod()+" Request received: "+request.getRequestURL());
        List<String> headers = Collections.list((Enumeration<String>)request.getHeaderNames());
        log.debug("|-- Headers : "+ headers.size()); 

        for (String headerName : headers){
            log.debug("|    |-- "+headerName+" : "+ request.getHeader(headerName));
        }

        List<String> requestParameterNames = Collections.list((Enumeration<String>)request.getParameterNames());
        log.debug("|-- Parameters  : " + requestParameterNames.size()); 

        for (String parameterName : requestParameterNames){
            log.debug("|    |-- "+parameterName + " : " + request.getParameter(parameterName));
        }
        //log.debug("|-- Body  : " + request.get); 

        

    }
}
