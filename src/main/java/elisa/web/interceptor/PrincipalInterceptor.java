package elisa.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.ldap.userdetails.InetOrgPerson;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * This interceptor handle principal information.
 * In particular, it retrieves username and full-name 
 * and append the two information as servlet request attibutes to be
 * available to controller for further processing.
 *  
 * @author lucamag
 *
 */
public class PrincipalInterceptor  extends HandlerInterceptorAdapter {

    private static final Log log = LogFactory.getLog(PrincipalInterceptor.class);

    //before the actual handler will be executed
    @Override
    public boolean preHandle(HttpServletRequest request, 
            HttpServletResponse response, Object handler)
                    throws Exception {
        
        String username = getUserName();
        String displayname = getDisplayName(request);
        
        //Setting user information as HTTP request attribute to be available
        // to the Controller for further processing
        request.setAttribute("full-name",displayname);
        request.setAttribute("username",username);
        
        log.info("Username: "+username+", Full-name: "+displayname);

        return true;
    }
    
    
    
    
    /**
     * Return the full name of the user. "NOBODY" if there is no security context.
     * The logic depends on the auth mechanism, LDAP or SSO.
     * @param HTTPServletRequest request
     * @return the full user name
     */
    private String getDisplayName(HttpServletRequest request) {
        
        String displayName="NOBODY"  ; 
        if(SecurityContextHolder.getContext()!=null&&SecurityContextHolder.getContext().getAuthentication()!=null){
            Object principal=SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            //SSO authentication
            if(principal instanceof User){
            	//SSO authentication
                if(request.getHeader("X-forwarded-fullname")!=null){
                	displayName=request.getHeader("X-forwarded-fullname");
                	log.debug("SSO authentication for: "+displayName);
                }
                //special API user authentication 
                else {
                	displayName="Generic API user";
                	log.debug("API User authentication for: "+displayName);
                }
            } else if(principal instanceof InetOrgPerson) { //LDAP auth
                displayName=((InetOrgPerson)principal).getDisplayName();
                log.debug("LDAP authentication for: "+displayName);
            }
        }
        
        return displayName;
    }


    /**
     * Return username, "NOBODY" if there is no security context
     * The logic depends on the auth mechanism, LDAP or SSO.
     * @return the username
     */
    private String getUserName() {
        if(SecurityContextHolder.getContext()!=null&&SecurityContextHolder.getContext().getAuthentication()!=null){
            return SecurityContextHolder.getContext().getAuthentication().getName();
        }
        return "NOBODY";
    }

}
