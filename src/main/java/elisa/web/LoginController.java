package elisa.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
@RequestMapping("/auth")
public class LoginController {
	
	private static final Log log = LogFactory.getLog(LoginController.class);
	
	
	/**
	  * Handles and retrieves the login JSP page
	  * 
	  * @return the name of the JSP page
	  */
	 @RequestMapping(value = "/login", method = RequestMethod.GET)
	 public String getLoginPage(@RequestParam(value="error", required=false) boolean error, 
	   ModelMap model) {
	  log.debug("Received request to show login page");
	  return "loginpage";
	 }
	
	
	 /**
	  * Handles and retrieves the denied JSP page. This is shown whenever a regular user
	  * tries to access an admin only page.
	  * 
	  * @return the name of the JSP page
	  */
	 @RequestMapping(value = "/denied", method = RequestMethod.GET)
	  public String getDeniedPage() {
	  log.debug("Received request to show denied page");
	   
	  // This will resolve to /WEB-INF/jsp/deniedpage.jsp
	  return "deniedpage";
	 }
	
	
}
